<%@ page session="false"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<%--   <link rel="stylesheet" href="<c:url value='/static/css/tutorial.css'/>" type="text/css" /> --%>
<title>Sparking Banking System</title>

</head>
<body>
	<jsp:include page="index.jsp" />
	<div>
		<ul id="mycontent">

			<li><br />
				<form name="form" method="post" action="MerchantAccountAuthorize">
					<br> <b> <c:out value="${message}" /></b> <br /> <br /> <font
						size="5" color="blue"><b><a>Account Details</a></b></font> <br />
					<font size="3" color="blue"><a>Available Balance: </a></font>
					${balance}<br /> <br />
					<a>Account ID: </a></font> ${accountId}<br /> <br />
					<table border="2" width=300 height=100 align="left">
						<tr>
							<th colspan="5">ID</th>
							<th colspan="5">Date</th>
							<th colspan="5">Type</th>
							<th colspan="5">Amount</th>
						</tr>
						<c:forEach items="${transactionList}" var="transaction">
							<tr>
								<td colspan="5"><c:out value="${transaction.transactionId}" /></td>
								<td colspan="5"><c:out value="${transaction.timeStamp}" /></td>
								<td colspan="5"><c:out value="${transaction.type}" /></td>
								<td colspan="5"><c:out value="${transaction.amount}" /></td>
							</tr>
						</c:forEach>
						<tr>
							<td align="center"><button type="submit">Authorize
									Verification</button></td>
						</tr>
					</table>
				</form></li>
		</ul>
	</div>

</body>
</html>
