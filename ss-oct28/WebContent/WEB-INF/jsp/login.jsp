<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="net.tanesha.recaptcha.ReCaptcha"%>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<body onload='document.f.j_username.focus();'>

	<h1 style="text-align: center;">SPARKY'S ONLINE BANKING</h1>



	<c:if test="${not empty error}">
		<div class="errorblock">
			Your login attempt was not successful, try again.<br /> Caused :
			${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
		</div>
	</c:if>

	<form name='f' action="<c:url value='j_spring_security_check' />"
		method='POST'>

		<table>
			<tr>
				<td>User:</td>
				<td><input type='text' name='j_username' value=''></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type='password' name='j_password' /></td>
			</tr>
			<tr>
				<td colspan='2'><input name="submit" type="submit"
					value="submit" /></td>
			</tr>
			<tr>
				<td colspan='2'><input name="reset" type="reset" /></td>
			</tr>
			
		</table>


		<%
			ReCaptcha c = ReCaptchaFactory.newReCaptcha(
					"6Ld0v-kSAAAAABkP623aSQN5fPt0wemWk6J5u5WV",
					"6Ld0v-kSAAAAAMNWdA79kWKOZ7rrkB0DZNXNLveX", false);
			out.print(c.createRecaptchaHtml(null, null));
		%>
	</form>
	<form method="post">
	<table>
	<tr>
				<td colspan='2'><input name="Forgot Password" type="submit"
					value="Forgot password"
					onClick="this.form.action='${pageContext.servletContext.contextPath}/ForgotPW_username_page'" /></td>
	</tr></table>
	</form>
</body>
</html>