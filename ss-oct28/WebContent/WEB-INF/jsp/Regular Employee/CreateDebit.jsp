<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create Transaction</title>
</head>
<body>
<jsp:include page="RegularEmployee.jsp"></jsp:include>
<br/><br/>
<h4>Enter below details to debit from the authorized account.</h4>
<form:form modelAttribute="transactionModel" method="POST" action="${pageContext.servletContext.contextPath}/EmployeeTransactions_CreateDebit">
<p> "Your Account ID :" <c:out value="${accountId}"/></p>
	<table>
	<tr>
	   <td><form:label path="receiverId">Receiver Account ID:</form:label></td>
	   <td><form:input path="receiverId"/></td>
	  </tr>
	   <tr>
	   <td><form:label path="amount">Amount:</form:label></td>
	   <td><form:input path="amount"/></td>
	  </tr>
	</table>
	<br>
	<input type="submit" value="Add Debit">
</form:form>

</body>
</html>