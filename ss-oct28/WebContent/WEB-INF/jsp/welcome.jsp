<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<sec:authorize access="hasRole('Admin')">
				<a href="WelcomeSystemAdministration" >System Administration</a>
</sec:authorize>

<sec:authorize access="hasRole('Employee')">
				<a href="WelcomeEmployeeManagement" >Employee
					Management</a>
</sec:authorize>

<sec:authorize access="hasRole('Manager')">
				<a href="DepartmentManager" >Manager</a>
</sec:authorize>

<sec:authorize access="hasRole('Corporate')">
				<a href="WelcomeCorporateOfficial" >Corporate Official</a>
</sec:authorize>

<sec:authorize access="hasRole('User')">
				<a href="WelcomeIndividualUser" >Individual User</a>
</sec:authorize>
<sec:authorize access="hasRole('Merchant')">
				<a href="WelcomeMerchant" >Merchant</a>
</sec:authorize>

</body>
</html>