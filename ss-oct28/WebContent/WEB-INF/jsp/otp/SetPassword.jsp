<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Set Password</title>
</head>
<body>

	<br>
	<form name=setPassword method="POST" action="ForgotPW_final">
		<br> <b> <c:out value="${message}" />
		</b> <br /> <br />
		<table border="1">
			<tr>
				<td>Username:</td>
				<td>${username}<br></td>
			</tr>
			<tr>
				<td>Enter new Password:</td>
				<td><input type="password" name="newPW" ID="newPW"><br></td>
			</tr>
			<tr>
				<td>Confirm Password:</td>
				<td><input type="password" name="confirmPW" ID="confirmPW"><br></td>
			</tr>
		</table>
		<br> <br />
		<button type="submit">Submit</button>
		<br />
	</form>
</body>
</html>