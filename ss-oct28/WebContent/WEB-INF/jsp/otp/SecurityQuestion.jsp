<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Security Question</title>
</head>
<body>

	<br>
	<form:form modelAttribute="userModel" method="POST" action="ForgotPW_QA_check_Page">
	<br> <b> <c:out value="${message}" />
					</b> <br />
		<table>
			<tr>
				<td>Username:</td>
				<td>${username}<br></td>
			</tr>
			<tr>
			<tr>
				<td>Here is your security question:</td>
				<td>${question}<br></td>
			</tr>
			<tr>
				<td>Enter the Answer:</td>
				<td><input type="text" name="Answer" ID="Answer"><br></td>
			</tr>
			<tr>
			<form:hidden path="username" value="${username}"/>
			<form:hidden path="securityQuestion" value="${question}"/>
			<form:hidden path="answer" value="Answer"/>
			</tr>
		</table>
		<br>
		<input type="submit" value="submit">
	</form:form>
</body>
</html>