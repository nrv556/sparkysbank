<%@ page session="false"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<%--   <link rel="stylesheet" href="<c:url value='/static/css/tutorial.css'/>" type="text/css" /> --%>
<title>Sparking Banking System</title>

<!--The CSS code.-->
<style type="text/css" media="screen">
#mymenu {
	margin: 0;
	padding: 0;
}

#mymenu li {
	margin: 0;
	padding: 0;
	list-style: none;
	float: left;
}

#mymenu li a {
	display: block;
	margin: 0 1px 0 0;
	padding: 4px 10px;
	width: 80px;
	background: #bbbaaa;
	color: #ffffff;
	text-align: center;
}

#mymenu li a:hover {
	background: #aaddaa
}

#mymenu ul {
	position: absolute;
	visibility: hidden;
	margin: 0;
	padding: 0;
	background: #eeebdd;
	border: 1px solid #ffffff
}

#mymenu ul a {
	position: relative;
	display: block;
	margin: 0;
	padding: 5px 10px;
	width: 80px;
	text-align: left;
	background: #eeebdd;
	color: #000000;
}

#mymenu ul a:hover {
	background: #aaffaa;
}
</style>

<script type="text/javascript">
	//variables declaration
	var timer = 0;
	var item = 0;
	//function
	// for opening of submenu elements
	function openelement(num) {
		//checks whether there is an open submenu and makes it invisible 

		visibilitycheck();
		//shows the chosen submenu element
		item = document.getElementById(num);
		item.style.visibility = 'visible';

	}
	function visibilitycheck() {
		if (item != 0) {
			item.style.visibility = 'hidden';
		}
	}
	// function for closing of submenu elements
	function closeelement() {
		//closes the open submenu elements and loads the timer with 500ms
		timer = window.setTimeout(visibilitycheck(), 500);
	}

	//function for keeping the submenu loaded after the end of the 500 ms timer
	function keepsubmenu() {
		window.clearTimeout(timer);
	}

	//hides the visualized menu after clicking outside of its area and expiring of the loaded
	//timerdocument.onclick = closeelement();
</script>
<!--END of CSS code-->
</head>
<body>
	<div id="content">
		<h1>Sparky Banking System</h1>
		<!--HTML code for the menu -->
		<ul id="mymenu">
			<li><a href="UserAccount" onmouseout="closeelement()">Account</a></li>
			<li><a href="UserTransferPage" onmouseout="closeelement()">Transfer</a></li>

			<li><a href="UserCreditDebitPage" onmouseout="closeelement()">Credit/debit</a></li>
			<li><a href="UserPayBillsPage" onmouseout="closeelement()">PayBills</a></li>
			<li><a href="UserAuthPage" onmouseout="closeelement()">Authorize</a></li>
			<li><a href="#" onmouseover="openelement('menu3')">Profile</a>
				<ul id="menu3" onmouseover="keepsubmenu()"
					onmouseout="closeelement()">
					<li><a href="UserChangePWPage"
						onmouseover="menu3.style.visibility='visible';">Change
							Password</a> <a href="UserUpdateProfilePage"
						onmouseover="menu3.style.visibility='visible';">Update Contact
							Information</a></li>
				</ul></li>
			<li><a
				href="<c:url value="${pageContext.servletContext.contextPath}/j_spring_security_logout"/>">Logout</a></li>
		</ul>
	</div>
	<div>
		<ul id="mycontent">

			<li><br />
				<form name="form" method="post" action="UserAccountAuthorize">
					<br> <b> <c:out value="${message}" />
					</b> <br /> <br /> <br /> <font size="5" color="blue"><b><a>Account
								Details</a></b></font> <br /> <font size="3" color="blue"><a>Available
							Balance: </a></font> ${balance}<br /> <br /><a>Account ID:
							 </a></font> ${accountId}<br /> <br />
					<table border="2" width=300 height=100 align="left">
						<tr>
							<th colspan="5">ID</th>
							<th colspan="5">Date</th>
							<th colspan="5">Type</th>
							<th colspan="5">Amount</th>
						</tr>
						<c:forEach items="${transactionList}" var="transaction">
							<tr>
								<td colspan="5"><c:out value="${transaction.transactionId}" /></td>
								<td colspan="5"><c:out value="${transaction.timeStamp}" /></td>
								<td colspan="5"><c:out value="${transaction.type}" /></td>
								<td colspan="5"><c:out value="${transaction.amount}" /></td>
							</tr>
						</c:forEach>
						<tr>
							<td align="center"><button type="submit">Authorize
									Verification</button></td>
						</tr>
					</table>


				</form></li>
		</ul>
	</div>

</body>
</html>
