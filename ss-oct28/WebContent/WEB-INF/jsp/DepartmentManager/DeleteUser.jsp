<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="c" 

           uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Delete User</title>

</head>

<body>

<br/>

<jsp:include page="DepartmentManager.jsp" />

<br/>



<table border="2" width=300  height=100 align=center>

<tr><th colspan="5">Username</th>

<th colspan="5">DepartmentID</th>

<!-- <th colspan="5">Salary</th>

<th colspan="5">Authorization</th></tr> -->

<c:forEach items="${emp}" var="emp">

  <tr>

   <td colspan="5"><c:out value="${emp.username}" /></td>

<%-- <td colspan="5"><c:out value="${emp.name}" /></td> --%>

   <td colspan="5"><c:out value="${emp.departmentId}" /></td>


    <form:form method="POST" modelAttribute="employeeDummy" commandName="employeeDummy" action="${pageContext.servletContext.contextPath}/DepartmentManager/Delete_Employee/"> 
 	  <td colspan="8"><input type="submit" value="Delete"/></td>

  	 <form:hidden path="username" value="${emp.username}" />


 </form:form>  </tr>

 </c:forEach>

 </table>

  ${message}

</body>

</html>