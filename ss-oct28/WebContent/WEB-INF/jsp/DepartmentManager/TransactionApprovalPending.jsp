<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Transaction Updated</title>
</head>
<body>
<jsp:include page="DepartmentManager.jsp"></jsp:include>
<p>Transaction pending under Corporate employee.</p>

<br/>
<a href="${pageContext.servletContext.contextPath}/DepartmentManager">Go to Home</a>

</body>
</html>