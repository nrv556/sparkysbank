<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Transactions</title>
</head>
<body>
<br/>
<jsp:include page="DepartmentManager.jsp" />
<form method ="post" action= "DepartmentManager/ViewTransactions">
<br/>
<br/>
<table border="1">
<tr>
<td>Transaction ID</td>
<td>Amount</td>
<td>Receiver ID</td>
<td>Sender ID</td>
<td>Status</td>
<td>TimeStamp</td>
</tr>

<c:forEach var="Activities" items="${msg}">
<tr>
<td>${Activities.getTransactionId()}</td>
<td>${Activities.getAmount()}</td>
<td>${Activities.getReceiverId()}</td>
<td>${Activities.getSenderId()}</td>
<td>${Activities.getStatus()}</td>
<td>${Activities.getTimeStamp()}</td>
</tr>

</c:forEach>
</table>
</form>
</body>
</html>