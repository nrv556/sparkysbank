<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Support</title>

</head>
<body>
<h2>Log File</h2>
<jsp:include page="SystemAdministrator.jsp" />
<br/><br>
<b>
<c:out value="${message}"/>
</b>
 <br>
 <table border="1">
<tr>
<td>LogID</td>
<td>Description</td>
<td>Activity Time</td>
</tr>

<c:forEach var="Activities" items="${log}">
<tr>
<td>${Activities.getLogID()}</td>
<td>${Activities.getDescription()}</td>
<td>${Activities.getActivityTime()}</td>
</tr>

</c:forEach>
</table>

</body>
</html>