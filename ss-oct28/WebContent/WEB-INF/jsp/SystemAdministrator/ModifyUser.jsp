<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Modify User</title>
<script>
function validateForm()
{
var x=document.forms["modifyUser"]["UserID"].value;
if (x==null || x=="")
  { 
  alert("Please enter UserID");
  return false;
  }
}
</script>
</head>
<body>
<jsp:include page="ManageUserAccounts.jsp" />
<br/>
<br>
<form name="modifyUser" method ="post" action= "SystemModifyUserSubmit.html">
<br>
<b>
<c:out value="${message}"/>
</b>
<br/>
<br/>
<table border="1">
<tr><td>User ID:</td><td><input type="text" name="UserID" id="UserID"><br></td></tr>
<tr>
<td>DepartmentId:</td>
<td><select name="DepartmentId" ID="DepartmentId">
<option value="0">--</option>
<option value="1">Sales</option>
<option value="2">HR</option>
<option value="3">IT</option>
<option value="4">Transactions</option>
<option value="5">Management</option>
</select><br></td>
</tr>
<tr><td>Salary (Enter Null if no salary update):</td><td><input type="text" name="salary"><br></td></tr>
<tr><td><button type="submit">Modify</button></td> </tr>
</table>
</form>

</body>
</html>