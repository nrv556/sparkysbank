<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Verify Requests</title>
<script>
function validateForm()
{
var x=document.forms["SystemVerifyRequests"]["UserID"].value;
if (x==null || x=="")
  {
  alert("First name must be filled out");
  return false;
  }
}
</script>



</head>
<body>
<jsp:include page="SystemAdministrator.jsp" />
<br/><br>
<form name="SystemVerifyRequests" method ="post" onsubmit="return validateForm();" action= "SystemVerifyUserSubmit.html">
<br>
<b>
<c:out value="${message}"/>
</b><br/>
<b>
Authorization provided Employees request list</b>
<br><br>
 <table border="1">
<tr>
<td>UserID</td>
<td>Name</td>
<td>Role</td>
</tr>

<c:forEach var="Rolelist" items="${requests}">
<tr>
<td>${Rolelist.getUsername()}</td>
<td>${Rolelist.getName()}</td>
<td>${Rolelist.getRole()}</td>
</tr>

</c:forEach>
</table>
<br><b>Assign Roles</b><br>
<table border="1">
<tr><td>Enter User ID:</td><td><input type="text" name="UserID" ID="UserID"><br></td></tr>
<tr>
<td>Role:</td>
<td><select name="Role" ID="Role">
<option value="Employee">Employee</option>
<option value="Admin">Admin</option>
<option value="Manager">Manager</option>
<option value="Corporate">Corporate</option>
</select><br></td>
</tr>
<tr><td><button type="submit">Assign Role</button></td></tr>
</table>
</form>
</body>
</html>