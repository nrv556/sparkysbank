<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add User</title>
</head>
<body>

<jsp:include page="ManageUserAccounts.jsp" />
<br>
<form name= addUser method="POST" action="SystemAddInternalUserSubmit.html">
<br>
<b>
<c:out value="${message}"/>
</b>
<br/>
<br/>
<table border="1">
<tr>
<td>User ID:</td>
<td><input type="text" name="UserID" ID="UserID"><br></td>
</tr>
<tr>
<td>Name:</td>
<td><input type="text" name="Name" ID="Name"><br></td>
</tr>
<tr>
<td>Password:</td>
<td><input type="password" name="Password" ID="Password"><br></td>
</tr>
<tr>
<td>DateOfBirth:</td>
<td><input type="text" name="dateOfBirth" ID="dateOfBirth"><br></td>
</tr>
<tr>
<td>Email:</td>
<td><input type="text" name="email" ID="email"><br></td>
</tr>
<tr>
<td>Address:</td>
<td><input type="text" name="address" ID="address"><br></td>
</tr>
<tr>
<td>Phone Number:</td>
<td><input type="text" name="phoneNumber" ID="phoneNumber"><br></td>
</tr>

<tr>
<td>Security Question:</td>
<td><input type="text" name="securityQuestion" ID="securityQuestion"><br></td>
</tr>
<tr>
<td>Answer:</td>
<td><input type="text" name="Answer" ID="Answer"><br></td>
</tr>
<tr>
<td>Role:</td>
<td><select name="Role" ID="Role">
<option value="Employee">Employee</option>
<option value="Admin">Admin</option>
<option value="Manager">Manager</option>
<option value="Corporate">Corporate</option>
</select><br></td>
</tr>
<tr>
<td>DepartmentId:</td>
<td><select name="DepartmentId" ID="DepartmentId">
<option value="1">Sales</option>
<option value="2">HR</option>
<option value="3">IT</option>
<option value="4">Transactions</option>
<option value="5">Management</option>
</select><br></td>
</tr>
<tr>
<td>Salary:</td>
<td><input type="text" name="Salary" ID="Salary"><br></td>
</tr>
</table>

 <br>
<br/>
<button type="submit">Submit</button>
<br/>
</form>
</body>
</html>