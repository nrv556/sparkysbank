<%@ page session="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
  <head>
      <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <%--   <link rel="stylesheet" href="<c:url value='/static/css/tutorial.css'/>" type="text/css" /> --%>
      <title>Corporate Official</title>
      
<!--The CSS code.-->
<style type="text/css" media="screen">
#mymenu { margin: 0; padding: 0; }
#mymenu li { margin: 0; padding: 0;
list-style: none; float: left; }
#mymenu li a { display: block; margin: 0 1px 0 0; padding: 4px 10px; width: 120px height : 20px; background:
#bbbaaa; color: #ffffff; text-align: center; }
#mymenu li a:hover { background: #aaddaa }
#mymenu ul { position: absolute; visibility: hidden; margin: 0; padding: 0; background:
#eeebdd; border: 1px solid #ffffff }
#mymenu ul a { position: relative; display: block; margin: 0; padding: 5px 10px; width: 150px;
text-align: left; background: #eeebdd; color: #000000; }
#mymenu ul a:hover { background: #aaffaa; }
</style>

<script type="text/javascript">
//variables declaration
var timer = 0;
var item = 0;
//function
// for opening of submenu elements
function openelement(num){
//checks whether there is an open submenu and makes it invisible 

visibilitycheck();
//shows the chosen submenu element
item = document.getElementById(num);
item.style.visibility = 'visible'; 

}
function visibilitycheck(){
	if(item!=0) 
	{item.style.visibility = 'hidden';
	}
}
// function for closing of submenu elements
function closeelement(){
//closes the open submenu elements and loads the timer with 500ms
timer = window.setTimeout(visibilitycheck(),500); 
}

//function for keeping the submenu loaded after the end of the 500 ms timer
function keepsubmenu()
{
	window.clearTimeout(timer); 
}

//hides the visualized menu after clicking outside of its area and expiring of the loaded
//timerdocument.onclick = closeelement();


</script>
<!--END of CSS code-->
  </head>
<body>
 <br>
<b>
<c:out value="${message}"/>
</b>
<br/>
<div id="content">
<h1>Corporate Level Management Official</h1>
<!--HTML code for the menu -->
<ul id="mymenu">
<li><a href="#" onmouseover="openelement('menu1')">Manage Internal User Accounts</a>
<ul id ="menu1" onmouseover="keepsubmenu()" onmouseout="closeelement()">
<li><a href="${pageContext.servletContext.contextPath}/WelcomeCorporateOfficial/CorporateAddUser" onmouseover = "menu1.style.visibility = 'visible';">Add user</a>
<a href="${pageContext.servletContext.contextPath}/WelcomeCorporateOfficial/CorporateDeleteUser"onmouseover = "menu1.style.visibility = 'visible';">Delete User</a>
<a href="${pageContext.servletContext.contextPath}/WelcomeCorporateOfficial/CorporateTransferUser"onmouseover = "menu1.style.visibility = 'visible';">Transfer User</a>
</li>
</ul>
</li>


<li> 
<a href="${pageContext.servletContext.contextPath}/WelcomeCorporateOfficial/CorporateAuthorize" onmouseover="closeelement()">Authorize</a>
</li>
<li> 
<a href="${pageContext.servletContext.contextPath}/WelcomeCorporateOfficial/CorporateViewAccountInfo" onmouseover="closeelement()">View Account Info</a>
</li>
<li> 
<a href="${pageContext.servletContext.contextPath}/WelcomeCorporateOfficial/CorporateViewTransactions" onmouseover="closeelement()">View Transactions</a>
</li>
<li> 
<a href="${pageContext.servletContext.contextPath}/WelcomeCorporateOfficial/CorporateViewPII" onmouseover="closeelement()">View PII</a>
</li>
<li> 
<a href="<c:url value="${pageContext.servletContext.contextPath}/j_spring_security_logout"/>">Logout</a>
</li>
</ul>
</div>
</body>
</html>
