<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Modify Transactions</title>

</head>
<body>
<jsp:include page="CorporateManager.jsp"></jsp:include> <br/> <br/>
<form:form method="POST" modelAttribute="transaction" action="${pageContext.servletContext.contextPath}/Employee_UpdateTransaction" >
<br>
<b>
<c:out value="${message}"/>
</b>
<br/>
<br/>
<h4>Modify Transactions</h4>
<table align="center" border="1" cellpadding="5" cellspacing="5">
<c:forEach items="${transaction}" var="transaction">
   <tr><td bgcolor="#CCCCCC" width="150">Transaction ID</td><td width="150"><c:out value="${transaction.transactionId}"/></td></tr>
   <tr><td bgcolor="#CCCCCC" width="150">Sender Acc No.</td><td width="150"><c:out value="${transaction.senderId}" /></td></tr>
   <tr><td bgcolor="#CCCCCC" width="150">Receiver Acc No.</td><td width="150"><c:out value="${transaction.receiverId}" /></td></tr>
   <tr><td bgcolor="#CCCCCC" width="150">Modified Amount : </td><td width="150"><input type="text" value="${transaction.amount}" maxlength="10" name="updateAmount" ID="updateAmount"></td></tr>
</c:forEach>
</table>
<tr><td><input type="submit" value="Update Transaction" align="middle"/></td></tr>

</form:form>

<%-- <form>
<br/>
<table border="1">
<tr>
<td>Amount:</td>
<td><input type="text" name="Amount"><br></td>
</tr>
<tr>
<td>Type:</td>
<td><input type="text" name="Debit/Credit"><br></td>
</tr>
</table>
<p>
Enter the Debit/Credit Account in the second text box?
 <br>
<br/>
<button type="button">Confirm</button>
</form>--%>
</body>
</html> 