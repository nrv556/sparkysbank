package com.securebank.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.securebank.model.Customers;
import com.securebank.model.Merchant;
import com.securebank.model.Merchant;

public class MerchantDAO {

	private HibernateTemplate hibernateTemplate;

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public String enableAuth(String username) throws HibernateException {
		String msg = null;
		try {
			Merchant merchant = new Merchant();
			Session session = hibernateTemplate.getSessionFactory()
					.openSession();
			String hql = "from Merchant as merchant where merchant.username=:username";
			Query query = session.createQuery(hql);
			query.setString("username", username);
			List<Merchant> list = query.list();
			java.util.Iterator<Merchant> iter = list.iterator();
			while (iter.hasNext()) {
				merchant = iter.next();
			}
			merchant.setAuthorization("enabled");

			try {
				session.beginTransaction();
				session.saveOrUpdate(merchant);
				session.getTransaction().commit();
				msg = "PII Authorization enabled succesfully.";
				session.close();
			} catch (Exception e) {
				msg = "PII Authorization enabled failed due to some reason.";
				System.out.println("Exception in commiting : " + e);
				session.getTransaction().rollback();
				session.clear();
			}
		} catch (Exception e) {
			msg = "Could not enable PII Authorization";
		}
		return msg;
	}

	public String disableAuth(String username) throws HibernateException {
		String msg = null;
		try {
			/*Merchant merchant = new Merchant();
			Session session = hibernateTemplate.getSessionFactory()
					.openSession();
			String hql = "from Merchant as merchant where merchant.username=:username";
			Query query = session.createQuery(hql);
			query.setString("username", username);
			List<Merchant> list = query.list();
			java.util.Iterator<Merchant> iter = list.iterator();
			while (iter.hasNext()) {
				merchant = iter.next();
			}
			merchant.setAuthorization("disabled");

			try {
				session.beginTransaction();
				session.saveOrUpdate(merchant);
				session.getTransaction().commit();
				msg = "PII Authorization disabled succesfully.";
				session.close();
			} catch (Exception e) {
				msg = "PII Authorization disabled failed due to some reason.";
				System.out.println("Exception in commiting : " + e);
				session.getTransaction().rollback();
				session.clear();
			}

		} catch (Exception e) {
			msg = "Could not disable PII Authorization";
		}
		return msg;*/
			Merchant merchant = new Merchant();
			Session session = hibernateTemplate.getSessionFactory().openSession();
			String hql = "from Merchant as merchant where merchant.username=:username";
			Query query = session.createQuery(hql);
			query.setString("username", username);
			List<Merchant> list = query.list();
			Iterator<Merchant> iter = list.iterator();
			while (iter.hasNext()) {
				merchant = iter.next();
			}
			merchant.setAuthorization("disabled");

			try {
				session.beginTransaction();
				session.saveOrUpdate(merchant);
				session.getTransaction().commit();
				msg = "PII Authorization disabled succesfully.";
				session.close();
			} catch (Exception e) {
				msg = "PII Authorization could not be disabled";
				System.out.println("Exception in commiting : " + e);
				session.getTransaction().rollback();
				session.clear();
			}
			}
			catch(Exception e)
			{
				msg = "PII Authorization could not be disabled";
			}
			return msg;
	}
}
