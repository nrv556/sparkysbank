package com.securebank.dao;

import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.securebank.model.Account;
import com.securebank.model.Customers;
import com.securebank.model.Employee;
import com.securebank.model.Transaction;
import com.securebank.model.User;
import com.securebank.web.CorporateOfficialController;

@Transactional
@Repository
public class CorporateDAO {

	@Autowired
	private HibernateTemplate hibernateTemplate;
	@Autowired
	private Transaction transaction;
	@Autowired
	private Account account;

	public void setAccount(Account account) {
		this.account = account;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	@SuppressWarnings("unused")
	private static final Logger logger = Logger
			.getLogger(CorporateDAO.class);


	public String addUser(String UserID, String Name, String Password,
			String dateOfBirth, String email, String address, long phoneNumber,
			String securityQuestion, String Answer, String Role)
			throws HibernateException {
		User user = new User();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			java.util.Date date = new java.util.Date(System.currentTimeMillis()
					- (24 * 3600 * 1000));
			Timestamp timestamp = new Timestamp(date.getTime());
			user.setUsername(UserID);
			user.setAddress(address);
			user.setAccountStatus("open");
			user.setAccountType("savings");
			user.setDateOfBirth(dateOfBirth);
			user.setEmail(email);
			user.setLastLoginTime(timestamp);
			user.setName(Name);
			user.setPassword(Password);
			user.setPhoneNumber(phoneNumber);
			user.setRole(Role);
			user.setSecurityQuestion(securityQuestion);
			user.setAnswer(Answer);
			try {
				session.beginTransaction();
				session.save(user);
				session.getTransaction().commit();
			} catch (HibernateException hibernateException) {
				logger.info("Exception in adding user by Corpotate Official :" +hibernateException);			
			}
			} catch (HibernateException hibernateException) {
			session.getTransaction().rollback();
			logger.info("Exception in adding user by corpotate Official :" +hibernateException);
			session.close();
		}
		session.close();
		return "Done";
	}

	public String createCustomer(String username) throws HibernateException {
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			Customers customers = new Customers();
			customers.setUsername(username);
			customers.setAuthorization("disabled");
			String sql = "SELECT * FROM account where username =  '" + username
					+ "'";
			SQLQuery query = session.createSQLQuery(
					sql);
			query.addEntity(Account.class);
			// query.setParameter("username", username);
			Account account = (Account) query.list().get(0);

			customers.setAccountId(account.getAccountId());
			session.beginTransaction();
			session.save(customers);
			session.getTransaction().commit();
			session.close();
		} catch (HibernateException hibernateException) {
			System.out.println("Exception in commiting : " + hibernateException);
			logger.info("Exception in creating customer by Corpotate Official :" +hibernateException);	
			session.close();
		}
		session.close();
		return "Done";
	}

	public String createAccount(String username, String Role,
			Double savingAccount) throws HibernateException {
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			Account account = new Account();

			account.setUsername(username);
			// account.setAccountType("savings");
			account.setBalance(savingAccount);
			account.setUserType(Role);
			session.beginTransaction();
			session.save(account);
			session.getTransaction().commit();
			session.close();
		} catch (HibernateException hibernateException) {
			System.out.println("Exception in commiting : " + hibernateException);
			logger.info("Exception in creating account by Corpotate Official :" +hibernateException);		}
			session.close();
		return "Done";
	}

	public String createEmployee(String username, Integer departmentId,
			double salary) throws HibernateException {
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			Employee employee = new Employee();
			employee.setUsername(username);
			employee.setDepartmentId(departmentId);
			employee.setSalary(salary);
			employee.setAuthorization("disabled");
			session.beginTransaction();
			session.save(employee);
			session.getTransaction().commit();
			session.close();
		} catch (HibernateException hibernateException) {
			logger.info("Exception in creating employeee by Corpotate Official :" +hibernateException);		
			session.close();
		}
		return "Done";
	}

	public void deleteUser(String username) {
		Session session = hibernateTemplate.getSessionFactory().openSession();
		User usr = (User) session.load(User.class, username);
		if (null != usr) {
			try {
				session.beginTransaction();
				session.delete(usr);
				session.getTransaction().commit();
				session.close();
			}
			catch(HibernateException hibernateException) {
				logger.info("Exception while deleting user by corporate manager: " +hibernateException);
				session.close();
			}
		}
	}

	public String CorporateUpdateInternalUser(String username,
			String departmentId, String salary) {
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			String sql = "SELECT * FROM employee where username =  '"
					+ username + "'";
			SQLQuery query = session.createSQLQuery(
					sql);
			query.addEntity(Employee.class);
			Employee emp = (Employee) query.list().get(0);
			
			if (Integer.parseInt(departmentId) != 0) {
				emp.setDepartmentId(Integer.parseInt(departmentId));
			}
			if (!salary.equalsIgnoreCase("Null")) {
				emp.setSalary(Double.parseDouble(salary));
			}
			session.beginTransaction();
			session.update(emp);
			session.getTransaction().commit();
			session.close();
		} catch (HibernateException hibernateException) {
			logger.info("Exception in update internel user by Corpotate Official :" +hibernateException);hibernateException.printStackTrace();
			session.close();
			return "error";
		}

		return "done";
	}

	@SuppressWarnings("unchecked")
	public List<Account> viewAccountInfo(String userID) {
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			List<Account> acc = null;
			String sql = "select * from Account having username in(select username from Employee where departmentID =(select departmentId from employee where username = :username))";
			SQLQuery query = session.createSQLQuery(
					sql);
			query.setString("username", userID);
			System.out.println(userID);
			query.addEntity(Account.class);
			acc = (List<Account>) query.list();
			session.close();
			return acc;
		} catch (HibernateException hibernateException) {
			logger.info("Exception in viewing Account Information by Corporate Official :" +hibernateException);
			session.close();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Transaction> viewTransactions(String userID) {
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			List<Transaction> tran = null;
			String sql = "select * from transaction having (senderId in(select accountID from account where username in(select username from employee where departmentID =(select departmentId from employee where username = :username)))) or (receiverId in(select accountID from account where username in(select username from employee where departmentID =(select departmentId from employee where username = :username))))";
			SQLQuery query = session.createSQLQuery(
					sql);
			System.out.println("I'm checking for transactions query");
			query.setString("username", userID);
			System.out.println(userID);
			query.addEntity(Transaction.class);
			tran = (List<Transaction>) query.list();
			session.close();
			return tran;
		} catch (HibernateException hibernateException) {
			logger.info("Exception in viewing transactions by Corpotate Official :" +hibernateException);
			session.close();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public List<User> viewPII() {
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			List<User> pii = null;
			String sql = "select * from User having username in (select username from Customers where authorization = :param)";

			SQLQuery query = session.createSQLQuery(
					sql);
			query.setString("param", "Enabled");
			System.out.println("I'm checking for userPII");
			query.addEntity(User.class);
			pii = (List<User>) query.list();
			session.close();
			return pii;
		} catch (HibernateException hibernateException) {
			logger.info("Exception in viewing PII by Corpotate Official :" +hibernateException);
			session.close();
			return null;
		}

	}

	public List<Transaction> authorize() {
		// TODO Auto-generated method stub
		List<Transaction> transactionsList = null;
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			String hql = "from Transaction as transaction where transaction.status=:statusVal";
			Query query = session.createQuery(hql);
			query.setString("statusVal", "corporate_processing");
			transactionsList = query.list();
			
			Iterator<Transaction> transactionIterator = transactionsList.iterator();
			while (transactionIterator.hasNext()) {
				transaction = transactionIterator.next();
				logger.info("Transaction Id: "+ transaction.getTransactionId());
			}
		}	
		catch (HibernateException hibernateException) {
			logger.info("View Transaction by Corporate Manager Exception :" + hibernateException);
		}
		return transactionsList;
	}

	public Transaction approveTransaction(Integer transactionId) {
		// TODO Auto-generated method stub
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			transaction = (Transaction)session.load(Transaction.class, transactionId);
			double amount = transaction.getAmount();
			if (amount > 8000 && amount <= 10000) {
				transaction.setStatus("processed");
				transaction.setAmount(amount);
				List<Transaction> transactionList = null;
				String hqlAccId;
				hqlAccId = "from Transaction where transactionId = :transactionId";
				Query retreiveAccId = session.createQuery(hqlAccId);
				retreiveAccId.setLong("transactionId", transactionId);
				transactionList = retreiveAccId.list();
				debitSenderAccount(transactionList.get(0).getSenderId(), amount);
				creditReceiverAccount(transactionList.get(0).getReceiverId(), amount);
			}
			else {
				transaction.setStatus("rejected");
			}
			session.beginTransaction();
			session.update(transaction);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("deleteTransaction unsuccessful " + hibernateException);
			session.getTransaction().rollback();
			session.clear();
		}
		return transaction;
	}
	
	private void debitSenderAccount(long senderId, double updateAmount) {
		// TODO Auto-generated method stub
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			List<Account> accountList = null;

			//Retrieve Checking Account Balance of Receiver ID
			String hqlRetreiveReceiverCheckingAccountBalance = "from Account where accountId = :senderAccountId";
			Query queryRetreiveReceiverCheckingAccountBalance = session.createQuery(hqlRetreiveReceiverCheckingAccountBalance);
			queryRetreiveReceiverCheckingAccountBalance.setLong("senderAccountId", senderId);
			
			accountList = queryRetreiveReceiverCheckingAccountBalance.list();
			
			double senderCheckingBalance = accountList.get(0).getBalance();
			senderCheckingBalance -= updateAmount;
			account = (Account)session.load(Account.class, senderId);
			account.setBalance(senderCheckingBalance);
			
			//Updating Receiver Account checking balance.
			session.beginTransaction();
			session.update(account);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("Receiver Account update not done with exception"+ hibernateException);
			session.getTransaction().rollback();
			session.clear();
		}
	}

	private void creditReceiverAccount(long receiverId, double updateAmount) {
		// TODO Auto-generated method stub
		Session session = hibernateTemplate.getSessionFactory().openSession();
		List<Account> accountList = null;
		try {
			//Retrieve Account Balance of Receiver ID
			String hqlRetreiveReceiverAccountBalance;
			hqlRetreiveReceiverAccountBalance = "from Account where accountId = :receiverAccountId";
			Query queryRetreiveReceiverAccountBalance = session.createQuery(hqlRetreiveReceiverAccountBalance);
			queryRetreiveReceiverAccountBalance.setLong("receiverAccountId", receiverId);
			
			accountList = queryRetreiveReceiverAccountBalance.list();
			
			double receiverCheckingBalance = accountList.get(0).getBalance();
			receiverCheckingBalance += updateAmount;
			account = (Account)session.load(Account.class, receiverId);
			account.setBalance(receiverCheckingBalance);
			
			//Updating Receiver Account checking balance.
			session.beginTransaction();
			session.update(account);
			session.getTransaction().commit();
			session.close();
		}
		catch(HibernateException hibernateException) {
			logger.info("Sender Account update not done with exception"+ hibernateException);
			session.getTransaction().rollback();
			session.clear();
		}
	}
}
