package com.securebank.dao;

import java.util.Date;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.securebank.model.Otp;
import com.securebank.model.User;

public class OtpDAO {
	private HibernateTemplate hibernateTemplate;

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public boolean saveOtp(String username, String otp_str, Date date) {
		boolean result = false;
		Otp otp = new Otp();
		otp.setDate(date);
		otp.setOtp(otp_str);
		otp.setUsername(username);
		Session session = hibernateTemplate.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			session.saveOrUpdate(otp);
			session.getTransaction().commit();
			result = true;
			session.close();
		} catch (Exception e) {
			result = false;
			System.out.println("Exception in commiting : " + e);
			session.getTransaction().rollback();
			session.clear();
		}
		return result;
	}

	public String getOtp(String username) {
		String otp_str = null;
		String msg = null;
		Otp otp = new Otp();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		String hql = "from Otp as otp where otp.username=:username";
		Query query = session.createQuery(hql);
		query.setString("username", username);
		List<Otp> list = query.list();
		java.util.Iterator<Otp> iter = list.iterator();
		while (iter.hasNext()) {
			otp = iter.next();
		}
		if (otp.getUsername() == null) {
			msg = "The username doesn't exist.";
		} else {
			otp_str = otp.getOtp();
		}
		return otp_str;
	}

	public Date getGenDate(String username) {
		Date genDate = null;
		String msg = null;
		Otp otp = new Otp();
		Session session = hibernateTemplate.getSessionFactory().openSession();
		String hql = "from Otp as otp where otp.username=:username";
		Query query = session.createQuery(hql);
		query.setString("username", username);
		List<Otp> list = query.list();
		java.util.Iterator<Otp> iter = list.iterator();
		while (iter.hasNext()) {
			otp = iter.next();
		}
		if (otp.getUsername() == null) {
			msg = "The username doesn't exist.";
		} else {
			genDate = otp.getDate();
		}
		return genDate;
	}
	
}
