package com.securebank.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.securebank.model.Customers;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.securebank.model.Account;
import com.securebank.model.Employee;
import com.securebank.model.Merchant;
import com.securebank.model.Systemadmin;
import com.securebank.model.User;

@Transactional
@Repository
public class SystemAdminDAO {

	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public String addUser(String UserID, String Name, String Password,
			String dateOfBirth, String email, String address, long phoneNumber,
			String accountType, String securityQuestion, String Answer,
			String Role) throws HibernateException {
		User user = new User();
		String message = null;
		Session session = sessionFactory.openSession();

		try {
			System.out.println("In DAO DEBUG" + email);
			java.util.Date date = new java.util.Date(System.currentTimeMillis()
					- (24 * 3600 * 1000));
			System.out.println("In DAO DEBUG2" + address);
			Timestamp timestamp = new Timestamp(date.getTime());
			System.out.println("In DAO DEBUG3" + address);
			user.setUsername(UserID);
			user.setAddress(address);
			user.setAccountStatus("open");
			user.setAccountType(accountType);
			user.setDateOfBirth(dateOfBirth);
			user.setEmail(email);
			user.setLastLoginTime(timestamp);
			user.setName(Name);
			user.setPassword(Password);
			user.setPhoneNumber(phoneNumber);
			user.setRole(Role);
			user.setSecurityQuestion(securityQuestion);
			user.setAnswer(Answer);
			/*
			 * user.setOTP("1211"); String currenttime=timestamp.toString();
			 * user.setCurrenttime(currenttime);
			 */

			System.out.println("In DAO DEBUG4" + accountType);

			System.out.println("In DAO DEBUG5" + Role);

			System.out.println("In DAO DEBUG6" + securityQuestion);

			session.beginTransaction();
			session.saveOrUpdate(user);
			session.getTransaction().commit();
			// sessionFactory.getCurrentSession().save(user);

			System.out.println("In DAO DEBUG7" + UserID);
			// session.getTransaction().commit();
			System.out.println("In DAO DEBUG8" + Name);
			// session.close();
			System.out.println("In DAO DEBUG9" + email);

			createLogFile("UserID:" + UserID
					+ " added as User to the application");
			message = "User added successfully";
			session.close();
			// sessionFactory.getCurrentSession().close();

		} catch (Exception e) {
			System.out.println("In DAO exception");
			message = "User is not added. Please try again";
			e.printStackTrace();
			session.getTransaction().rollback();
			session.clear();
			// sessionFactory.getCurrentSession().close();
		}
		return message;
	}

	public String createCustomer(String username) throws HibernateException {
		String message = null;
		Session session = sessionFactory.openSession();
		try {
			Customers customers = new Customers();
			customers.setUsername(username);
			customers.setAuthorization("Disabled");
			String sql = "SELECT * FROM account where username =  '" + username
					+ "'";

			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Account.class);
			// query.setParameter("username", username);
			Account account = (Account) query.list().get(0);

			customers.setAccountId(account.getAccountId());

			session.beginTransaction();
			session.saveOrUpdate(customers);
			session.getTransaction().commit();

			createLogFile("UserID:" + username
					+ " added as customer to the application");
			message = "User added successfully";
			session.close();
		} catch (Exception e) {
			System.out.println("Exception in commiting : " + e);
			message = "Customer is not added";
			e.printStackTrace();
			session.getTransaction().rollback();
			session.clear();
		}
		return message;
	}

	public String createMerchant(String username) throws HibernateException {
		String message = null;
		Session session = sessionFactory.openSession();
		try {
			Merchant merchant = new Merchant();
			System.out.println("MERCHANT111");
			merchant.setUsername(username);
			merchant.setAuthorization("Disabled");
			String sql = "SELECT * FROM account where username =  '" + username
					+ "'";
			System.out.println("MERCHANT222" + username);
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Account.class);
			Account account = (Account) query.list().get(0);
			System.out.println("MERCHANT333");
			merchant.setAccountId(account.getAccountId());

			session.beginTransaction();
			session.saveOrUpdate(merchant);
			session.getTransaction().commit();

			System.out.println("MERCHANT444");
			createLogFile("UserID:" + username
					+ " added as Merchant to the application");
			System.out.println("MERCHANT555");
			session.close();
			message = "User added successfully";
		} catch (Exception e) {
			System.out.println("Exception in commiting : " + e);
			System.out.println("MERCHANT666");
			message = "Merchant is not added";
			e.printStackTrace();
			session.getTransaction().rollback();
			session.clear();

		}
		return message;
	}

	public String createAccount(String username, String Role,
			Double savingAccount) throws HibernateException {
		String message = null;
		Session session = sessionFactory.openSession();
		try {
			Account account = new Account();

			account.setUsername(username);
			account.setBalance(savingAccount);
			account.setUserType(Role);

			session.beginTransaction();
			session.saveOrUpdate(account);
			session.getTransaction().commit();

			createLogFile("Account created for UserID:" + username);
			message = "User added successfully";
			session.close();

		} catch (Exception e) {
			System.out.println("Exception in commiting : " + e);
			message = "Account is not created for the user";
			e.printStackTrace();
			session.getTransaction().rollback();
			session.clear();
		}
		return message;
	}

	public String createEmployee(String username, Integer departmentId,
			double salary) throws HibernateException {
		String message = null;
		Session session = sessionFactory.openSession();
		try {
			Employee employee = new Employee();
			employee.setUsername(username);
			employee.setDepartmentId(departmentId);
			employee.setSalary(salary);
			employee.setAuthorization("Disabled");

			session.beginTransaction();
			session.saveOrUpdate(employee);
			session.getTransaction().commit();

			createLogFile("UserID:" + username
					+ " added as Employee to the application");
			message = "Internal User added successfully";
			session.close();
		} catch (Exception e) {
			System.out.println("Exception in commiting : " + e);
			message = "Employee is not created";
			e.printStackTrace();
			session.getTransaction().rollback();
			session.clear();
		}
		return message;
	}

	public String getRole(String username) throws HibernateException {
		Session session = sessionFactory.openSession();
		try {

			String sql = "SELECT * FROM user where username =  '" + username
					+ "'";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(User.class);
			// query.setParameter("username", username);
			User user = (User) query.list().get(0);
			String role = user.getRole();
			session.close();
			return role;

		} catch (Exception e) {
			System.out.println("Exception in commiting : " + e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.clear();
			return "Invalid UserID";

		}
	}

	public long getaccountId(String username) throws HibernateException {
		Session session = sessionFactory.openSession();
		try {

			String sql = "SELECT * FROM account where username =  '" + username
					+ "'";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Account.class);
			Account account = (Account) query.list().get(0);
			long accountid = account.getAccountId();
			session.close();
			return accountid;

		} catch (Exception e) {
			System.out.println("Exception in commiting : " + e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.clear();
			long a = 0;
			return a;

		}

	}

	public String deleteUser(String username) throws HibernateException {
		String message = null;
		Session session = sessionFactory.openSession();
		try {

			String sql = "SELECT * FROM user where username =  '" + username
					+ "'";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(User.class);
			User user = (User) query.list().get(0);

			session.beginTransaction();
			session.delete(user);
			session.getTransaction().commit();
			message = "User Deleted successfully";
			session.close();

		} catch (Exception e) {
			System.out.println("Exception in commiting : " + e);
			e.printStackTrace();
			session.getTransaction().rollback();
			session.clear();
			message = "Invalid UserID";
		}
		return message;
	}

	public String deleteCustomer(String username) throws HibernateException {
		String message = null;
		Session session = sessionFactory.openSession();
		try {

			String sql = "SELECT * FROM customers where username =  '"
					+ username + "'";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Customers.class);
			Customers cust = (Customers) query.list().get(0);

			session.beginTransaction();
			session.delete(cust);
			session.getTransaction().commit();

			createLogFile("Customer with UserID:" + username
					+ " deleted from the application");
			message = "Customer Deleted successfully";
			session.close();

		} catch (Exception e) {
			System.out.println("Exception in commiting : " + e);
			message = "Customer could not be deleted";
			session.getTransaction().rollback();
			session.clear();
			e.printStackTrace();
		}
		return message;
	}

	public String deleteMerchant(String username) throws HibernateException {
		String message = null;
		Session session = sessionFactory.openSession();
		try {

			String sql = "SELECT * FROM merchant where username =  '"
					+ username + "'";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Merchant.class);
			Merchant merc = (Merchant) query.list().get(0);

			session.beginTransaction();
			session.delete(merc);
			session.getTransaction().commit();

			createLogFile("Merchant with UserID:" + username
					+ " deleted from the application");
			message = "Merchant Deleted successfully";
			session.close();
		} catch (Exception e) {
			System.out.println("Exception in commiting : " + e);
			message = "Merchant could not be deleted";
			session.getTransaction().rollback();
			session.clear();
			e.printStackTrace();
		}
		return message;
	}

	public String deleteEmployee(String username) throws HibernateException {
		String message = null;
		Session session = sessionFactory.openSession();
		try {

			String sql = "SELECT * FROM employee where username =  '"
					+ username + "'";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Employee.class);
			Employee emp = (Employee) query.list().get(0);

			session.beginTransaction();
			session.delete(emp);
			session.getTransaction().commit();
			session.close();
			createLogFile("Employee with UserID:" + username
					+ " deleted from the application");
			message = "Employee Deleted successfully";
		} catch (Exception e) {
			System.out.println("Exception in commiting : " + e);
			e.printStackTrace();
			message = "Employee could not be deleted";
			session.getTransaction().rollback();
			session.clear();
		}
		return message;
	}

	public String deleteManager(String username) throws HibernateException {

		try {

		} catch (Exception e) {
			System.out.println("Exception in commiting : " + e);
			e.printStackTrace();
		}
		return "message";
	}

	public String updateInternalUser(String username, String departmentId,
			String salary) {
		Session session = sessionFactory.openSession();
		String message = null;
		try {
			String sql = "SELECT * FROM employee where username =  '"
					+ username + "'";
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Employee.class);
			Employee emp = (Employee) query.list().get(0);
			if (Integer.parseInt(departmentId) != 0) {
				emp.setDepartmentId(Integer.parseInt(departmentId));
			}
			if (salary.equalsIgnoreCase("Null") || salary.isEmpty()) {
	
			}
			else
			{
				emp.setSalary(Double.parseDouble(salary));
			}

			session.beginTransaction();
			session.saveOrUpdate(emp);
			session.getTransaction().commit();
			session.close();

			createLogFile("Information updated for Employee with UserID:"
					+ username);
			message = "Internal User updated successfully";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Invalid userID";
			session.getTransaction().rollback();
			session.clear();
		}
		return message;
	}

	@SuppressWarnings("unchecked")
	public List<Systemadmin> accessLogfile() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		try {
			List<Systemadmin> logdata = new ArrayList<Systemadmin>();

			String sql = "Select * FROM systemadmin";

			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(Systemadmin.class);
			logdata = (List<Systemadmin>) query.list();
			session.close();
			return logdata;
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.clear();
			return null;
		}
	}

	public String createLogFile(String Description) {
		Systemadmin admin = new Systemadmin();
		Session session = sessionFactory.openSession();
		java.util.Date date = new java.util.Date(System.currentTimeMillis()
				- (24 * 3600 * 1000));

		Timestamp timestamp = new Timestamp(date.getTime());
		admin.setDescription(Description);
		admin.setActivityTime(timestamp);
		try {

			session.beginTransaction();
			session.saveOrUpdate(admin);
			session.getTransaction().commit();
			session.close();
			return "successful";
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.clear();
			return "LogFile was not generated";
		}

	}

	@SuppressWarnings("unchecked")
	public List<User> accessRequests() {
		// TODO Auto-generated method stub
		Session session = sessionFactory.openSession();
		try {

			List<User> dataRequests = new ArrayList<User>();

			String sql = "Select * FROM user where username in(select username from employee where authorization='Enabled')";

			SQLQuery query = session.createSQLQuery(sql);

			query.addEntity(User.class);
			dataRequests = (List<User>) query.list();
			session.close();
			return dataRequests;
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.clear();
			return null;
		}

	}

	public String updateUserRole(String username, String role) {
		// TODO Auto-generated method stub
		String message = null;
		Session session = sessionFactory.openSession();
		try {
			String sql1 = "select * from employee where username= '" + username
					+ "'";
			SQLQuery query = session.createSQLQuery(sql1);
			query.addEntity(Employee.class);
			Employee emp = (Employee) query.list().get(0);

			if (emp.getAuthorization().equalsIgnoreCase("enabled")) {
				String sql = "select * from user where username= '" + username
						+ "'";
				SQLQuery query1 = session.createSQLQuery(sql);
				query1.addEntity(User.class);
				User user = (User) query1.list().get(0);

				user.setRole(role);

				session.beginTransaction();
				session.update(user);
				session.getTransaction().commit();
				session.close();

				message = "Employee Role updated successfully";
			} else {
				message = "You are Unauthorized to change role of this user";
			}

			createLogFile("Information updated for Employee with UserID:"
					+ username);
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
			session.clear();
			return "Invalid UserID";
		}
		return message;

	}

}
