package com.securebank.web;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
//import org.hibernate.mapping.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.securebank.model.Account;
import com.securebank.model.Employee;
import com.securebank.model.Transaction;
import com.securebank.model.User;
import com.securebank.service.DepartmentService;
import com.securebank.service.EmployeeService;

/**
 * 
 * @author Sandeep
 * 
 */
@Controller
public class DepartmentController {

	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private Transaction transaction;
	
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public void setDepartmentService(DepartmentService departmentService) {
		this.departmentService = departmentService;
	}

	public void setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	@Autowired
	private Employee employee;

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	private static final Logger logger = Logger
			.getLogger(DepartmentController.class);

	@RequestMapping(value = "/DepartmentManager/AddEmployee", method = RequestMethod.GET)
	public ModelAndView DepartmentManager_AddEmployee() {
		Employee addEmployee = new Employee();
		ModelAndView model = new ModelAndView("/DepartmentManager/AddEmployee");
		model.addObject("addEmployee", addEmployee);
		// log it via log4j

		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS2");
		return model;
	}

	@RequestMapping("/DepartmentManager/AddInternalUserSubmit")
	public ModelAndView addInternalUserSubmit(@RequestParam String UserID,
			String Name, String Password, String dateOfBirth, String email,
			String address, String phoneNumber, String securityQuestion,
			String Answer, String Role, String DepartmentId, String Salary)
			throws HibernateException {
		String message = null;
		try {
			System.out.println("DEBUG" + Name);
			// if (Name != null) {

			if (UserID.isEmpty() || Name.isEmpty() || Password.isEmpty()
					|| dateOfBirth.isEmpty() || email.isEmpty()
					|| address.isEmpty() || phoneNumber.isEmpty()
					|| securityQuestion.isEmpty() || Answer.isEmpty()
					|| Role.isEmpty() || DepartmentId.isEmpty()
					|| Salary.isEmpty()) {
				message = "Please enter all the fields";
				return new ModelAndView("/DepartmentManager/AddEmployee",
						"message", message);
			} else if (!validatePassword(Password)) {
				message = "Please enter a proper password (Password length must be between 8-15 with one special character,uppercase and lowercase)";
				return new ModelAndView("/DepartmentManager/AddEmployee",
						"message", message);

			} else if (!validateEmail(email)) {
				message = "Please enter a valid email ID";
				return new ModelAndView("/DepartmentManager/AddEmployee",
						"message", message);

			} else if (!validateDate(dateOfBirth)) {
				message = "Please enter a valid date";
				return new ModelAndView("/DepartmentManager/AddEmployee",
						"message", message);
			} else if (!validate_Number(phoneNumber)) {
				message = "Please enter a valid Phone Number with 10 digits";
				return new ModelAndView("/DepartmentManager/AddEmployee",
						"message", message);

			}
			else if(Double.parseDouble(Salary)<0)
			{
				message="Please enter salary greater than zero";
				return new ModelAndView("/DepartmentManager/AddEmployee",
						"message", message);
				
			}

			else {
				System.out.println("test1");
				message = departmentService.addInternalUser(UserID, Name,
						Password, dateOfBirth, email, address, phoneNumber,
						securityQuestion, Answer, Role, DepartmentId, Salary);
				System.out.println("test2");
				System.out.println("test3");
				return new ModelAndView("/DepartmentManager/AddEmployee",
						"message", message);
			}
		} catch (Exception e) {
			message = "Internal User is not added";
			e.printStackTrace();
			return new ModelAndView("/DepartmentManager/AddEmployee",
					"message", message);
		}
	}

	/*
	 * departmentService.addInternalUser(UserID, Name, Password, dateOfBirth,
	 * email, address, phoneNumber, securityQuestion, Answer, Role,
	 * DepartmentId, Salary); message = "Successfully Added user"; return new
	 * ModelAndView("/DepartmentManager/AddEmployee", "message", message); } }
	 * 
	 * catch (Exception e) { e.printStackTrace(); return new
	 * ModelAndView("/accessDenied"); } return null; }
	 */

	@RequestMapping(value = "/DepartmentManager/Delete_User", method = RequestMethod.GET)
	public ModelAndView DepartmentDeleteUser(
			@ModelAttribute("emp") Employee emp,
			@ModelAttribute("employeeDummy") Employee employeeDummy) {
		ModelAndView model = new ModelAndView("/DepartmentManager/DeleteUser");
		model.addObject("emp", employeeService.displayResults());
		model.addObject("employeeDummy", employee);
		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS2");
		return model;
	}

	@RequestMapping(value = "/DepartmentManager/Delete_Employee", method = RequestMethod.POST)
	public ModelAndView DepartmentDeleteEmployee(
			@ModelAttribute("employeeDummy") Employee empl) {
	
		employeeService.deleteUser(empl.getUsername());
		departmentService.deleteUser(empl.getUsername());
		ModelAndView model = new ModelAndView(
				"redirect:/DepartmentManager/Delete_User");
		model.addObject("message", "User Successfully deleted");
		return model;
	}

	@RequestMapping(value = "/DepartmentManager/TransferUser", method = RequestMethod.GET)
	public ModelAndView DepartmentManager_TransferUser() {
		Employee transferUser = new Employee();
		ModelAndView model = new ModelAndView("/DepartmentManager/ModifyUser");
		model.addObject("transferUser", transferUser);
		// log it via log4j

		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS2");
		return model;
	}

	@RequestMapping("/DepartmentManager/TransferInternalUserSubmit")
	public ModelAndView updateUser_Submit(@RequestParam String UserID,
			String DepartmentId, String salary) throws HibernateException {
		String message = null;
		try {
			if (UserID.isEmpty()) {
				message = "Please enter the User Id";
				/*
				 * String msg = departmentService.modifyInternalUser(UserID,
				 * DepartmentId, salary);
				 */
				return new ModelAndView("/DepartmentManager/ModifyUser",
						"message", message);
				/*
				 * if (msg.equalsIgnoreCase("error")) { return new
				 * ModelAndView("/accessDenied");
				 */
			}

			/*
			 * return new ModelAndView("/DepartmentManager/ModifyUser",
			 * "message", null);
			 */
			else if (DepartmentId.equalsIgnoreCase("0")
					&& (salary.isEmpty() || salary.equalsIgnoreCase(null))) {
				message = "Please enter either DepartmentId or salary";
				return new ModelAndView("/DepartmentManager/ModifyUser",
						"message", message);
			}
			else if(Double.parseDouble(salary)<0)
			{
				message="Amount should be greater than zero";
				return new ModelAndView("/DepartmentManager/ModifyUser",
						"message", message);

			}
			else {
				message = departmentService.modifyInternalUser(UserID,
						DepartmentId, salary);
				return new ModelAndView("/DepartmentManager/ModifyUser",
						"message", message);
			}
		} catch (Exception e) {
			message = "Cannot update the user";
			e.printStackTrace();
			return new ModelAndView("/DepartmentManager/ModifyUser", "message",
					message);
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/DepartmentManager/ViewAccountInfo")
	public ModelAndView viewAccountInfo_submit(HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession httpSession = request.getSession();
		httpSession.setAttribute("userName", SecurityContextHolder.getContext()
				.getAuthentication().getName());
		String UserID = (String) httpSession.getAttribute("userName");
		List<Account> msg = null;
		try {
			if (UserID != null) {
				msg = (List<Account>) departmentService.viewAccountInfo(UserID);

				return new ModelAndView("/DepartmentManager/View_Account_Info",
						"msg", msg);
			} else {
				return new ModelAndView("/accessDenied");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("/accessDenied");
		}
	}

	@RequestMapping("/DepartmentManager/ViewTransactions")
	public ModelAndView viewTransactions_submit(HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession httpSession = request.getSession();
		httpSession.setAttribute("userName", SecurityContextHolder.getContext()
				.getAuthentication().getName());
		String UserID = (String) httpSession.getAttribute("userName");
		List<Transaction> msg = null;
		try {
			if (UserID != null) {
				msg = (List<Transaction>) departmentService
						.viewTransactions(UserID);

				return new ModelAndView("/DepartmentManager/ViewTransactions",
						"msg", msg);
			} else {
				return new ModelAndView("/accessDenied");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("/accessDenied");
		}
	}

	@RequestMapping("/DepartmentManager/ViewPII")
	public ModelAndView viewPII() {

		List<User> msg = null;
		try {
			msg = (List<User>) departmentService.viewPII();
			return new ModelAndView("/DepartmentManager/ViewPII", "msg", msg);
			// if (logger.isInfoEnabled())logger.info("ENTERED view PII");

		} catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("/accessDenied");
		}
	}

	public boolean validatePassword(String Password) {

		String check = new String(
				"^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,20}$");
		// String passwordNotContains= new String("^[^<>'\"/;`]*$");

		boolean status = Password.matches(check);
		if (status) {
			if (Password.length() < 8 && Password.length() > 15) {
				return false;
			}
			return true;
		}
		return false;
	}

	private boolean validateEmail(String email) {
		// TODO Auto-generated method stub
		String regEx = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";

		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(email);

		if (m.find()) {
			return true;
		}
		return false;
	}

	private boolean validateDate(String dateOfBirth) {
		// TODO Auto-generated method stub
		try {
			new SimpleDateFormat(dateOfBirth);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}

	}

	@SuppressWarnings("unused")
	private boolean validate_Number(String phoneNumber) {
		// TODO Auto-generated method stub
		try {
			// long number =
			Long.parseLong(phoneNumber);
			if (phoneNumber.length() == 10) {
				return true;
			}
			return false;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	/*
	 * Authorize changes
	 */
	@RequestMapping(value = "/DepartmentManager/ManagerAuthorize", method = RequestMethod.GET)
	public ModelAndView authorize(@ModelAttribute("transactionModel") Transaction transactionModel, ModelMap model) {
		List<Transaction> msg = null;
		try {
			msg = (List<Transaction>) departmentService.authorize();
			model.addAttribute("transactionModel", transaction);
			model.addAttribute("model2", msg);
			return new ModelAndView("DepartmentManager/TransactionList", model);
			// if (logger.isInfoEnabled())logger.info("ENTERED view PII");

		} catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("/accessDenied");
		}
		}
	
	@RequestMapping(value = "/DepartmentManager/ManagerAuthorize", method = RequestMethod.POST)
	public ModelAndView authorizePost(@ModelAttribute("transactionModel") Transaction transaction) {
		Transaction trans = departmentService.approveTransaction(transaction.getTransactionId());
		if(trans.getStatus().equals("processed"))
			return new ModelAndView("DepartmentManager/TransactionApproved");
		else 
			return new ModelAndView("DepartmentManager/TransactionApprovalPending");
	}
	 

}