package com.securebank.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WelcomePageController {

	@RequestMapping(value = "/WelcomeUser", method = RequestMethod.GET)
	public ModelAndView Welcome() {

		ModelAndView model = new ModelAndView(
				"/welcome");
		
		return model;
	}
@RequestMapping(value = "/WelcomeSystemAdministration", method = RequestMethod.GET)
public ModelAndView WelcomeSystemAdministration() {

	ModelAndView model = new ModelAndView(
			"/SystemAdministrator/SystemAdministrator");
	
	return model;
}

@RequestMapping(value = "/WelcomeEmployeeManagement", method = RequestMethod.GET)
public ModelAndView WelcomeEmployee(HttpServletRequest request,HttpServletResponse response) {
	HttpSession httpSession = request.getSession();
	httpSession.setAttribute("userName", SecurityContextHolder.getContext().getAuthentication().getName());
	ModelAndView model = new ModelAndView(
			"/Regular Employee/RegularEmployee");
	
	return model;
}
@RequestMapping(value = "/DepartmentManager", method = RequestMethod.GET)
public ModelAndView WelcomeManager() {

	ModelAndView model = new ModelAndView(
			"/DepartmentManager/DepartmentManager");
	
	return model;
}
@RequestMapping(value = "/WelcomeCorporateOfficial", method = RequestMethod.GET)
public ModelAndView WelcomeCorporateOfficial() {

	ModelAndView model = new ModelAndView(
			"/CorporateLevelManager/CorporateManager");
	
	return model;
}

@RequestMapping(value = "/WelcomeIndividualUser", method = RequestMethod.GET)
public ModelAndView WelcomeIndividualUser(HttpServletRequest request,
		HttpServletResponse response) {
	HttpSession httpSession = request.getSession();
	httpSession.setAttribute("userName", SecurityContextHolder.getContext()
			.getAuthentication().getName());
	ModelAndView model = new ModelAndView("/IndividualUser/index");

	return model;
}

@RequestMapping(value = "/WelcomeMerchant", method = RequestMethod.GET)
public ModelAndView WelcomeMerchant(HttpServletRequest request,
		HttpServletResponse response) {
	HttpSession httpSession = request.getSession();
	httpSession.setAttribute("userName", SecurityContextHolder.getContext()
			.getAuthentication().getName());
	ModelAndView model = new ModelAndView("/Merchant/index");

	return model;
}
@RequestMapping(value = "/login", method = RequestMethod.GET)
public ModelAndView login() {
	// log it via log4j

	ModelAndView model = new ModelAndView("/login");

	return model;
}

@RequestMapping(value = "/accessDenied", method = RequestMethod.GET)
public ModelAndView loginFailed() {
	// log it via log4j

	ModelAndView model = new ModelAndView("/accessDenied");
	return model;
}

@RequestMapping(value = "/logout", method = RequestMethod.GET)
public ModelAndView logout(ModelMap model) {
	// log it via log4j
	model.addAttribute("message", "You are logged out");
	ModelAndView modelAndView = new ModelAndView("/logout", model);
	return modelAndView;
}


@RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
public ModelAndView loginfailed(ModelMap model) {
	// log it via log4j
	ModelAndView modelAndView = new ModelAndView("/loginfailed", model);
	return modelAndView;
}

}