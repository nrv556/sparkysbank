package com.securebank.web;

import java.text.SimpleDateFormat;
import java.util.List;







import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.securebank.service.SystemAdminService;
//import com.securebank.service.UserService; 
import com.securebank.model.Systemadmin;
import com.securebank.model.User;

import org.hibernate.HibernateException;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

//import com.securebank.service.SystemAdminService;

@Controller
public class SystemAdminController {

	private SystemAdminService systemadminService;




	@Autowired

	public void setSystemadminService(SystemAdminService systemadminService) {
		this.systemadminService = systemadminService;
	}
	

	@RequestMapping("/SystemVerifyRequests")
	public ModelAndView SAVerifyRequests() {
		 List<User> requests = null;
		 String message=null;
			try{
				requests=systemadminService.accessRequests();
				if(requests.size()==0)
				{	message="No requests available";
					return new ModelAndView("/SystemAdministrator/VerifyRequests","message",message);
				
				}
				return new ModelAndView("/SystemAdministrator/VerifyRequests","requests",requests);
			}
			catch(Exception e)
			{
				message="Requests cannot be displayed";
				e.printStackTrace();
				return new ModelAndView("/SystemAdministrator/VerifyRequests","message",message);
			}
			

		
	}


	@RequestMapping("/SystemVerifyUserSubmit")
	public ModelAndView SystemVerifyUsersubmit(@RequestParam String UserID, String Role) {
		String message=null;
		
		try{
			if(!UserID.isEmpty())
			{
				if(!Role.isEmpty())
				{
					message=systemadminService.updateUserRole(UserID,Role);
				}
				else
				{
					message="Please select a Role";
				}
			}
			else
			{
				message="Please enter a UserID";
			}
			
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
			message="Role not updated";
		}
		return new ModelAndView("/SystemAdministrator/VerifyRequests","message", message);

	}

	@RequestMapping("/SystemManageUserAccounts")
	public ModelAndView SAManageUserAccounts() {
		return new ModelAndView("/SystemAdministrator/ManageUserAccounts",
				"message", "");
	}

	@RequestMapping("/SystemAccessLogFile")
	public ModelAndView SAAccessLogFile() throws HibernateException {
	     List<Systemadmin> logdata = null; 
	     String message=null;
		try{
			logdata=systemadminService.accessLogfile();
			if(logdata.size()==0)
			{
				message="No data in Logfile";
				return new ModelAndView("/SystemAdministrator/AccessLogFile","message",message);
			}
			 Systemadmin[] log= new Systemadmin[logdata.size()]; 
	          
		        for(int i=0; i<logdata.size();i++) 
		        { 
		            Systemadmin logfile= new Systemadmin(); 
		            logfile.setLogID(logdata.get(i).getLogID()); 
		            logfile.setDescription(logdata.get(i).getDescription()); 
		            logfile.setActivityTime(logdata.get(i).getActivityTime());
		            System.out.println(logdata.get(i).getActivityTime());
		            log[i] = logfile; 
		        } 
			return new ModelAndView("/SystemAdministrator/AccessLogFile","log",log);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			message="Logfile cannot be displayed";
			return new ModelAndView("/SystemAdministrator/AccessLogFile","message",message);
		
		}

	}


	@RequestMapping("/SystemAddUser")
	public ModelAndView SAAddUser() {
		return new ModelAndView("/SystemAdministrator/AddUser", "message", "");
	}
	@RequestMapping("/SystemAddInternalUser")
	public ModelAndView AddInternalUser() {
		return new ModelAndView("/SystemAdministrator/AddInternalUser","message", "");
	}

	@RequestMapping("/SystemDeleteUser")
	public ModelAndView SADeleteUser() {
		return new ModelAndView("/SystemAdministrator/DeleteUser", "message","");
	}

	@RequestMapping("/SystemModifyUser")
	public ModelAndView SAModifyUser() {
		return new ModelAndView("/SystemAdministrator/ModifyUser", "message","");
	}

	@RequestMapping("/SystemAdministrator")
	public ModelAndView SASystemAdministrator() {
		return new ModelAndView("/SystemAdministrator/SystemAdministrator","message", "");
	}

	/*@RequestMapping("/loggedout")
	public ModelAndView Logout() {
		return new ModelAndView("logout","message", "");
	}*/


	@RequestMapping("/SystemAddUserSubmit")
	public ModelAndView addUserSubmit(@RequestParam String UserID, String Name,
			String Password, String dateOfBirth, String email, String address,
			String phoneNumber,String securityQuestion,
			String Answer, String Role,String SavingAccount) throws HibernateException {
		String message;
		String accountType="savings";
		try {
			System.out.println("DEBUG" + Name);
			if (Name.isEmpty() || UserID.isEmpty() || Password.isEmpty() || dateOfBirth.isEmpty() || email.isEmpty() || address.isEmpty() || phoneNumber.isEmpty() || securityQuestion.isEmpty() || Answer.isEmpty() || SavingAccount.isEmpty())
			{
				message="Please enter all the fields";

				return new ModelAndView("/SystemAdministrator/AddUser", "message",message);
				
			}
			else if(!validatePassword(Password))
			{
				message="Please enter a proper password (Password length must be between 8-15 with one special character,uppercase and lowercase)";

				return new ModelAndView("/SystemAdministrator/AddUser", "message",message);
		
			}
			else if(!validateEmail(email))
			{
				message="Please enter a valid email ID";
				return new ModelAndView("/SystemAdministrator/AddUser", "message",message);
		
			}
			else if(!validateDate(dateOfBirth))
			{
				message="Please enter a valid date (format:mm/dd/yyyy)";
				return new ModelAndView("/SystemAdministrator/AddUser", "message",message);

			}
			else if(!validate_Number(phoneNumber))
			{
				message="Please enter a valid Phone Number with 10 digits";
				return new ModelAndView("/SystemAdministrator/AddUser", "message",message);

			}
			else if(Long.parseLong(SavingAccount)<0)
			{
				message="Please enter amount greater than 0";
				return new ModelAndView("/SystemAdministrator/AddUser", "message",message);
			}
			else{
				System.out.println("test1");
				long accountid=systemadminService.addUser(UserID, Name, Password, dateOfBirth,email, address, phoneNumber, accountType,securityQuestion, Answer, Role,SavingAccount);
				System.out.println("test2");
					System.out.println("test3");
					String message1=new String("User created successfully. Account id:"+accountid);
					return new ModelAndView("/SystemAdministrator/AddUser", "message",message1);
			}
		}
		catch (NumberFormatException e) {
			message="Please enter proper digits in number field";
			e.printStackTrace();
			return new ModelAndView("/SystemAdministrator/AddUser", "message",message);
			
		}
		catch (Exception e) {
			message="User is not added";
			e.printStackTrace();
			return new ModelAndView("/SystemAdministrator/AddUser", "message",message);
		}
		
	}

	




	@RequestMapping("/SystemAddInternalUserSubmit")
	public ModelAndView addInternalUserSubmit(@RequestParam String UserID, String Name,
			String Password, String dateOfBirth, String email, String address,
			String phoneNumber,String securityQuestion,
			String Answer, String Role,String DepartmentId,String Salary) throws HibernateException {
		String message=null;
		try {
			String accountType="savings";
			System.out.println("DEBUG" + Name);
			if (Name.isEmpty() || UserID.isEmpty() || Password.isEmpty() || dateOfBirth.isEmpty() || email.isEmpty() || address.isEmpty() || phoneNumber.isEmpty() || securityQuestion.isEmpty() || Answer.isEmpty() || Salary.isEmpty()) 
			{

				message="Please enter all the fields";
			return new ModelAndView("/SystemAdministrator/AddInternalUser", "message",message);
	
			}
			else if(!validatePassword(Password))
			{
				message="Please enter a proper password (Password length must be between 8-15 with one special character,uppercase and lowercase)";

				return new ModelAndView("/SystemAdministrator/AddUser", "message",message);
		
			}
			else if(!validateEmail(email))
			{
				message="Please enter a valid email ID";
				return new ModelAndView("/SystemAdministrator/AddUser", "message",message);
		
			}
			else if(!validateDate(dateOfBirth))
			{
				message="Please enter a valid date (format:mm/dd/yyyy)";
				return new ModelAndView("/SystemAdministrator/AddUser", "message",message);

			}
			else if(!validate_Number(phoneNumber))
			{
				message="Please enter a valid Phone Number with 10 digits";
				return new ModelAndView("/SystemAdministrator/AddUser", "message",message);

			}
			else if(Double.parseDouble(Salary)<0)
			{
				message="Please enter salary greater than 0";
				return new ModelAndView("/SystemAdministrator/AddUser", "message",message);
			}
			
			else{
				System.out.println("test1");
				long accountid =systemadminService.addInternalUser(UserID, Name, Password, dateOfBirth,
						email, address, phoneNumber, accountType,
						securityQuestion, Answer, Role,DepartmentId,Salary);
				System.out.println("test2");

				String message1=new String("User created successfully. Account id:"+accountid);
		
				return new ModelAndView("/SystemAdministrator/AddInternalUser", "message",message1);
		
				
			}

			
		}
		
		catch (NumberFormatException e) {
			message="Please enter proper digits in number field";
			e.printStackTrace();
			return new ModelAndView("/SystemAdministrator/AddInternalUser", "message",message);
			
		}
		catch (Exception e) {
			message="Internal User is not added";
			e.printStackTrace();
			return new ModelAndView("/SystemAdministrator/AddInternalUser", "message",message);
		}
		
	}

	@RequestMapping("/SystemDeleteUserSubmit")
	public ModelAndView SystemDeleteUsersubmit(@RequestParam String UserID) throws HibernateException
	{
		String message=null;
		try{

			if(UserID.isEmpty())
			{message="Please enter UserID";
				
			}
			else{
				message=systemadminService.deleteUser(UserID);
			}
			return new ModelAndView("/SystemAdministrator/DeleteUser", "message",message);
		}
		catch (Exception e) {
			message="User is not deleted";
			e.printStackTrace();
			return new ModelAndView("/SystemAdministrator/DeleteUser", "message",message);
		}

	}

	@RequestMapping("/SystemModifyUserSubmit") 
	public ModelAndView  updateUser_Submit(@RequestParam String UserID, String DepartmentId, String
			salary) throws HibernateException 
			{String message=null;
		try{
			if (UserID.isEmpty()) {
				message="Please enter UserId";
				return new ModelAndView("/SystemAdministrator/ModifyUser", "message",message);
			}
			if(DepartmentId.equalsIgnoreCase("0")) 
			{		
				if(salary.isEmpty() ||salary.equalsIgnoreCase(null)) 
			{
				message="Please enter either DepartmentId or salary";
				return new ModelAndView("/SystemAdministrator/ModifyUser", "message",message);
				
			}
				else if(Double.parseDouble(salary)<0)
				{
					message="Please enter salary greater than 0";
					return new ModelAndView("/SystemAdministrator/ModifyUser", "message",message);
				}
				else
				{
					message = systemadminService.modifyInternalUser(UserID, DepartmentId, salary);
					return new ModelAndView("/SystemAdministrator/ModifyUser", "message",message);
				}
			}
			if(!DepartmentId.equalsIgnoreCase("0") && (salary.isEmpty() ||salary.equalsIgnoreCase(null)))
			{
				message = systemadminService.modifyInternalUser(UserID, DepartmentId, salary);
				return new ModelAndView("/SystemAdministrator/ModifyUser", "message",message);
			}
			
			if(Double.parseDouble(salary)<0)
			{
				message="Please enter salary greater than 0";
				return new ModelAndView("/SystemAdministrator/ModifyUser", "message",message);
			}
			
			else{
				message = systemadminService.modifyInternalUser(UserID, DepartmentId, salary);
				return new ModelAndView("/SystemAdministrator/ModifyUser", "message",message);
			}
		
		}
		catch(Exception e){
			message="User is not updated";
			e.printStackTrace();
			return new ModelAndView("/SystemAdministrator/ModifyUser", "message",message);
		}
		}
	
	public boolean validatePassword(String Password)
	{

				String check = new String("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,20}$"); 
				//String passwordNotContains= new String("^[^<>'\"/;`]*$");
				
				boolean status = Password.matches(check) ;
				if(status){
					if(Password.length()<8 && Password.length()>15)
					{
						return false;
					}
					return true;
				}
				return false;
	}
	
	private boolean validateEmail(String email) {
		// TODO Auto-generated method stub
		 String regEx = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";
		 
		 Pattern p = Pattern.compile(regEx);
		 Matcher m = p.matcher(email);
		 
		 if(m.find()) 
		 {
		  return true;
		  }
		 return false;
		 }

	
	private boolean validateDate(String dateOfBirth) {
		// TODO Auto-generated method stub
		try {
		    new SimpleDateFormat(dateOfBirth);
		    return true;
		} catch (IllegalArgumentException e) {
		    return false;
		}
		
	}	  
	
	private boolean validate_Number(String phoneNumber) {
		// TODO Auto-generated method stub
			try{
				//long number = 
						Long.parseLong(phoneNumber);
				if(phoneNumber.length()==10)
				{
				return true;	
				}
				return false;
			}catch(NumberFormatException e)
			{
				return false;
			}
		}
	

}
