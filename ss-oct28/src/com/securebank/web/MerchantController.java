package com.securebank.web;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.securebank.model.Transaction;
import com.securebank.service.MerchantService;
import com.securebank.service.UserService;

@Controller
public class MerchantController {
	// get log4j handler
private static final Logger logger = Logger.getLogger(UserController.class);

	@Autowired
	private UserService userService;
	@Autowired
	private MerchantService merchantService;

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	public void setMerchantService(MerchantService merchantService) {
		this.merchantService = merchantService;
	}



	@RequestMapping(value = "/MerchantAccount", method = RequestMethod.GET)
	public ModelAndView UserAccount(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		String message=null;
		try{
		HttpSession httpSession = request.getSession();
		String userName = (String) httpSession.getAttribute("userName");
		if(!userName.equals(SecurityContextHolder.getContext().getAuthentication().getName()))
		{
			message="Access Denied";
			return new ModelAndView("/accessDenied", "message",message);
		}
		
		logger.info("UserName in User Controller :" + userName);
		double balance = userService.getBalance(userName);
		long accountId = userService.getAccountId(userName);
		List<Transaction> list = userService.getTransaction(userName);
		model.addAttribute("accountId", String.valueOf(accountId));
		model.addAttribute("balance", String.valueOf(balance));
		model.addAttribute("transactionList", list);
		message="Account Details";
		return new ModelAndView("/Merchant/Account", "message",message);
		}
		catch(Exception e)
		{
			message="Account Details could not be populated";
			return new ModelAndView("/Merchant/Account", "message",message);
		}
	}

	@RequestMapping(value = "/MerchantAccountAuthorize", method = RequestMethod.POST)
	public ModelAndView UserAccountAuthorize(HttpServletRequest request,
			HttpServletResponse responset, ModelMap model) 
	{
		String msg=null;
	try{	HttpSession httpSession = request.getSession();
		String userName = (String) httpSession.getAttribute("userName");
		if(!userName.equals(SecurityContextHolder.getContext().getAuthentication().getName()))
		{
			msg="Access Denied";
			return new ModelAndView("/accessDenied", "message",msg);
		}
		
		logger.info("UserName in User Controller :" + userName);
		List<Transaction> transactionList = userService
				.getTransaction(userName);
		msg = userService.authorizeAccount(transactionList);
		double balance = userService.getBalance(userName);
		List<Transaction> list = userService.getTransaction(userName);
		model.addAttribute("balance", String.valueOf(balance));
		model.addAttribute("transactionList", list);
		return new ModelAndView("/Merchant/Account", "message", msg);
	}
	catch(Exception e)
	{
		msg="Transactions could not be authorized";
		return new ModelAndView("/Merchant/Account", "message", msg);
	}
	}

	@RequestMapping(value = "/MerchantPayBills", method = RequestMethod.POST)
	public ModelAndView UserPayBills(HttpServletRequest request,
			HttpServletResponse response, @RequestParam String AccountNo,
			String Amount, String CompanyName) {
		String msg=null;
		try{
		HttpSession httpSession = request.getSession();
		String userName = (String) httpSession.getAttribute("userName");
		if(!userName.equals(SecurityContextHolder.getContext().getAuthentication().getName()))
		{
			msg="Access Denied";
			return new ModelAndView("/accessDenied", "message",msg);
		}
		if(CompanyName.isEmpty() || AccountNo.isEmpty() || Amount.isEmpty())
		{
			msg="Please enter all the fields";
			return new ModelAndView("/Merchant/PayBills", "message", msg);
			
		}
		else if(!validate_longNumber(AccountNo))
		{
			msg="AccountNo field should be in digits";
			return new ModelAndView("/Merchant/PayBills", "message", msg);
	
		}
		else if(!validate_doubleNumber(Amount))
		{
			msg="Amount field should be in digits";
			return new ModelAndView("/Merchant/PayBills", "message", msg);
	
		}
		
		logger.info("UserName in User Controller :" + userName);
		long accountno=Long.parseLong(AccountNo);
		double amount=Double.parseDouble(Amount);
		if(amount<0)
		{

			msg="Amount field should not be negative";
			return new ModelAndView("/Merchant/PayBills", "message", msg);
	
		}
		msg = userService.payBills(userName, accountno, amount,
				CompanyName);
		return new ModelAndView("/Merchant/PayBills", "message", msg);
		}
		catch(Exception e)
		{	msg="Error in paying bills";
			return new ModelAndView("/Merchant/PayBills", "message", msg);
		}
	}

	@RequestMapping(value = "/MerchantCredit", method = RequestMethod.POST)
	public ModelAndView UserCredit(HttpServletRequest request,
			HttpServletResponse response, @RequestParam String CreditAmount) {
		String msg=null;
		try{
		HttpSession httpSession = request.getSession();
		String userName = (String) httpSession.getAttribute("userName");
		if(!userName.equals(SecurityContextHolder.getContext().getAuthentication().getName()))
		{
			msg="Access Denied";
			return new ModelAndView("/accessDenied", "message",msg);
		}
		if(CreditAmount.isEmpty())
		{
			msg="Please enter Amount";
			return new ModelAndView("/Merchant/Credit_debit", "message", msg);
			
		}
		else if(!validate_doubleNumber(CreditAmount))
		{
			msg="Amount field should be in digits";
			return new ModelAndView("/Merchant/Credit_debit", "message", msg);
	
		}
		else if(Double.parseDouble(CreditAmount)<0)
		{
			msg="Amount should be greater than zero";
			return new ModelAndView("/Merchant/Credit_debit", "message", msg);
		}

		logger.info("UserName in User Controller :" + userName);
		double creditamount=Double.parseDouble(CreditAmount);
		msg = userService.credit(userName, creditamount);
		return new ModelAndView("/Merchant/Credit_debit",
				"creditMessage", msg);
		}
		catch(Exception e)
		{
			msg="Amount not credited";
			return new ModelAndView("/Merchant/Credit_debit",
					"creditMessage", msg);
		}
	}

	@RequestMapping(value = "/MerchantDebit", method = RequestMethod.POST)
	public ModelAndView UserDebit(HttpServletRequest request,
			HttpServletResponse response, @RequestParam String DebitAmount) {
		String msg = null;
		try{
		HttpSession httpSession = request.getSession();
		String userName = (String) httpSession.getAttribute("userName");
		if(!userName.equals(SecurityContextHolder.getContext().getAuthentication().getName()))
		{
			msg="Access Denied";
			return new ModelAndView("/accessDenied", "message",msg);
		}
		
		if(DebitAmount.isEmpty())
		{
			msg="Please enter Amount";
			return new ModelAndView("/Merchant/Credit_debit", "message", msg);
			
		}
		else if(!validate_doubleNumber(DebitAmount))
		{
			msg="Amount field should be in digits";
			return new ModelAndView("/Merchant/Credit_debit", "message", msg);
	
		}
		else if(Double.parseDouble(DebitAmount)<0)
		{
			msg="Amount should be greater than zero";
			return new ModelAndView("/Merchant/Credit_debit", "message", msg);
		}
		logger.info("UserName in User Controller :" + userName);
		double debitamount=Double.parseDouble(DebitAmount);

		msg = userService.debit(userName, debitamount);
		return new ModelAndView("/Merchant/Credit_debit", "debitMessage",
				msg);
		}
		catch(Exception e)
		{
			msg="Amount not credited";
			return new ModelAndView("/Merchant/Credit_debit", "debitMessage",
					msg);
		}
	}

	@RequestMapping(value = "/MerchantChangePW", method = RequestMethod.POST)
	public ModelAndView UserChangePW(HttpServletRequest request,
			HttpServletResponse response, @RequestParam String CurrentPassword,
			String NewPassword, String ConfirmPassword) {
		String msg = null;
		try{
		HttpSession httpSession = request.getSession();
		String userName = (String) httpSession.getAttribute("userName");
		if(!userName.equals(SecurityContextHolder.getContext().getAuthentication().getName()))
		{
			msg="Access Denied";
			return new ModelAndView("/accessDenied", "message",msg);
		}
		if(!NewPassword.equals(ConfirmPassword))
		{
			msg="New Password and Confirm Password field donot match";
			return new ModelAndView("/Merchant/ChangePassword", "message",msg);
		}
		else if(!validatePassword(NewPassword))
		{
			msg="Please enter a proper password (Password length must be between 8-15 with one special character,uppercase and lowercase)";
			return new ModelAndView("/Merchant/ChangePassword", "message",msg);

		}

		logger.info("UserName in User Controller :" + userName);
		msg = userService.changePW(userName, CurrentPassword, NewPassword,
				ConfirmPassword);

		return new ModelAndView("/Merchant/ChangePassword", "message",
				msg);
		}
		catch(Exception e)
		{
			msg="Password is not changed";
			return new ModelAndView("/Merchant/ChangePassword", "message",
					msg);
		}
	}

	@RequestMapping(value = "/MerchantChangePWPage", method = RequestMethod.GET)
	public ModelAndView UserChangePWPage(HttpServletRequest request,
			HttpServletResponse response) {
	
		ModelAndView model = new ModelAndView("/Merchant/ChangePassword");
		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS");
		return model;
	}

	@RequestMapping(value = "/MerchantUpdateProfilePage", method = RequestMethod.GET)
	public ModelAndView UserUpdateProfilePage(HttpServletRequest request,
			HttpServletResponse response) {
	
		ModelAndView model = new ModelAndView("/Merchant/UpdateProfile");
		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS");
		return model;
	}

	@RequestMapping(value = "/MerchantUpdateProfile", method = RequestMethod.POST)
	public ModelAndView UserUpdateProfile(HttpServletRequest request,
			HttpServletResponse response, @RequestParam String Password,String NewAddress, String NewEmail,
			String NewMobileNumber) {
		String msg=null;
		
		HttpSession httpSession = request.getSession();
		String userName = (String) httpSession.getAttribute("userName");
		if(!userName.equals(SecurityContextHolder.getContext().getAuthentication().getName()))
		{
			msg="Access Denied";
			return new ModelAndView("/accessDenied", "message",msg);
		}
		if(Password.isEmpty() || NewAddress.isEmpty() || NewEmail.isEmpty() || NewMobileNumber.isEmpty())
		{
			msg="Please enter all the fields";
			return new ModelAndView("/Merchant/UpdateProfile", "message", msg);
		}
		else if(!validateEmail(NewEmail))
		{
			msg="Please enter a valid email ID";
			return new ModelAndView("/Merchant/UpdateProfile", "message", msg);
		}
		else if(!validate_Number(NewMobileNumber))
		{
			msg="Please enter a valid Phone Number with 10 digits";
			return new ModelAndView("/Merchant/UpdateProfile", "message",msg);

		}
		
		
		logger.info("UserName in User Controller :" + userName);
		msg = userService.updateProfile(userName, Password, NewAddress, NewEmail,
				NewMobileNumber);
		ModelAndView model = new ModelAndView("/Merchant/UpdateProfile", "message", msg);
		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS");
		return model;
		}
		
	

	@RequestMapping(value = "/MerchantTransferPage", method = RequestMethod.GET)
	public ModelAndView UserTransferPage(HttpServletRequest request,
			HttpServletResponse response) {
		
		ModelAndView model = new ModelAndView("/Merchant/Transfer");
		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS");
		return model;
	}

	@RequestMapping(value = "/MerchantTransfer", method = RequestMethod.POST)
	public ModelAndView UserTransfer(HttpServletRequest request,
			HttpServletResponse response, @RequestParam String AccountNo,
			String Name, String Amount) {
		String msg=null;
		
		HttpSession httpSession = request.getSession();
		String userName = (String) httpSession.getAttribute("userName");
		if(!userName.equals(SecurityContextHolder.getContext().getAuthentication().getName()))
		{
			msg="Access Denied";
			return new ModelAndView("/accessDenied", "message",msg);
		}
		else if(Name.isEmpty())
		{
			msg="Please enter name field";
			return new ModelAndView("/Merchant/Transfer", "message", msg);
		}
			
		else if(!validate_longNumber(AccountNo))
		{
			msg="AccountNo field should be in digits";
			return new ModelAndView("/Merchant/Transfer", "message", msg);
	
		}
		else if(!validate_doubleNumber(Amount))
		{
			msg="Amount field should be in digits";
			return new ModelAndView("/Merchant/Transfer", "message", msg);
	
		}
	
		logger.info("UserName in User Controller :" + userName);
		double amount=Double.parseDouble(Amount);
		if(amount<0)
		{

			msg="Amount field should not be negative";
			return new ModelAndView("/Merchant/Transfer", "message", msg);
	
		}
		msg = userService.transfer(userName, AccountNo, Name, amount);
		ModelAndView model = new ModelAndView("/Merchant/Transfer",
				"message", msg);
		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS");
		return model;
	}

	@RequestMapping(value = "/MerchantCreditDebitPage", method = RequestMethod.GET)
	public ModelAndView UserCreditDebitPage(HttpServletRequest request,
			HttpServletResponse response) {
		
		ModelAndView model = new ModelAndView("/Merchant/Credit_debit");
		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS");
		return model;
	}

	@RequestMapping(value = "/MerchantOnlineTransferPage", method = RequestMethod.GET)
	public ModelAndView UserOnlineTransferPage(HttpServletRequest request,
			HttpServletResponse response) {
		
		ModelAndView model = new ModelAndView("/Merchant/OnlineTransfer");
		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS");
		return model;
	}

	@RequestMapping(value = "/MerchantAuthPage", method = RequestMethod.GET)
	public ModelAndView UserAuthPage(HttpServletRequest request,
			HttpServletResponse response) {
		
		ModelAndView model = new ModelAndView("/Merchant/Authorization");
		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS");
		return model;
	}

	@RequestMapping(value = "/MerchantPayBillsPage", method = RequestMethod.GET)
	public ModelAndView UserPayBillsPage(HttpServletRequest request,
			HttpServletResponse response) {
		
		ModelAndView model = new ModelAndView("/Merchant/PayBills");
		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS");
		return model;
	}

	@RequestMapping(value = "/MerchantEnableAuth", method = RequestMethod.POST)
	public ModelAndView UserEnableAuth(HttpServletRequest request,
			HttpServletResponse response) {
		String message=null;
		HttpSession httpSession = request.getSession();
		String userName = (String) httpSession.getAttribute("userName");
		if(!userName.equals(SecurityContextHolder.getContext().getAuthentication().getName()))
		{
			message="Access Denied";
			return new ModelAndView("/accessDenied", "message",message);
		}
		
		logger.info("UserName in User Controller :" + userName);
		message = merchantService.enableAuth(userName);
		ModelAndView model = new ModelAndView("/Merchant/Authorization","message",message);
		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS");
		return model;
	}

	@RequestMapping(value = "/MerchantDisableAuth", method = RequestMethod.POST)
	public ModelAndView UserDisableAuth(HttpServletRequest request,
			HttpServletResponse response) {
		String message=null;
		HttpSession httpSession = request.getSession();
		String userName = (String) httpSession.getAttribute("userName");
		if(!userName.equals(SecurityContextHolder.getContext().getAuthentication().getName()))
		{
			message="Access Denied";
			return new ModelAndView("/accessDenied", "message",message);
		}
		
		logger.info("UserName in User Controller :" + userName);
		message = merchantService.disableAuth(userName);
		ModelAndView model = new ModelAndView("/Merchant/Authorization","message",message);
		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS");
		return model;
	}
	private boolean validate_longNumber(String number) {
		// TODO Auto-generated method stub
			try{
				//long number = 
						Long.parseLong(number);
				
				return true;	
			}catch(Exception e)
			{
				return false;
			}
		}
	
	private boolean validate_doubleNumber(String number) {
		// TODO Auto-generated method stub
			try{
				//long number = 
						Double.parseDouble(number);
				
				return true;	
			}catch(Exception e)
			{
				return false;
			}
		}

	public boolean validatePassword(String Password)
	{

				String check = new String("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,20}$"); 
				//String passwordNotContains= new String("^[^<>'\"/;`]*$");
				
				boolean status = Password.matches(check) ;
				if(status){
					if(Password.length()<8 && Password.length()>15)
					{
						return false;
					}
					return true;
				}
				return false;
	}
	
	private boolean validateEmail(String email) {
		// TODO Auto-generated method stub
		 String regEx = "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b";
		 
		 Pattern p = Pattern.compile(regEx);
		 Matcher m = p.matcher(email);
		 
		 if(m.find()) 
		 {
		  return true;
		  }
		 return false;
		 }
	private boolean validate_Number(String phoneNumber) {
		// TODO Auto-generated method stub
			try{
				//long number = 
						Long.parseLong(phoneNumber);
				if(phoneNumber.length()==10)
				{
				return true;	
				}
				return false;
			}catch(NumberFormatException e)
			{
				return false;
			}
		}

}
