package com.securebank.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.securebank.model.Account;
import com.securebank.model.Employee;
import com.securebank.model.Transaction;
import com.securebank.model.User;
import com.securebank.service.EmployeeService;
import com.securebank.service.CorporateService;

@Controller

public class CorporateOfficialController {

	@Autowired	
	private Employee employee;
	
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Autowired	
	private Transaction transaction;
	
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	@Autowired	
	private EmployeeService employeeService;
	
	public void setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}
	
/*	@Autowired	
	private Transaction transaction;
	
	
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
*/
	@Autowired
	private CorporateService CorporateService;
	
	
	public void setCorporateService(CorporateService CorporateService) {
		this.CorporateService = CorporateService;
	}

	private static final Logger logger = Logger
			.getLogger(CorporateOfficialController.class);

	
	@RequestMapping(value = "/WelcomeCorporateOfficial/CorporateAddUser")
	public ModelAndView CorporateAddUser() {

		ModelAndView model = new ModelAndView(
				"/CorporateLevelManager/AddEmployee");
	Employee emp = new Employee();
	model.addObject("emp", emp);
	if (logger.isInfoEnabled())
		logger.info("ENTERED CONTROLLER CLASS2");
		return model;
		
	}
	
	
	
	@RequestMapping(value = "/WelcomeCorporateOfficial/CorporateAddEmployee", method = RequestMethod.POST)

	public ModelAndView CorporateAddEmployee(@RequestParam String UserID, String Name,
			String Password, String dateOfBirth, String email, String address,
			String phoneNumber,  String securityQuestion,
			String Answer, String Role,String DepartmentId,String Salary) throws HibernateException {
		String message;
		try {
			System.out.println("DEBUG" + Name);
			if (Name != null) {
				System.out.println("test1");
				CorporateService.addInternalUser(UserID, Name, Password, dateOfBirth,
						email, address, phoneNumber,
						securityQuestion, Answer, Role,DepartmentId,Salary);
				System.out.println("test2");
				message = "Successfully Added user";
				System.out.println("test3");
				return new ModelAndView(
						"CorporateLevelManager/AddEmployee", "message",
						message);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("/accessDenied");
		}
		return null;
	}


	@RequestMapping(value = "/WelcomeCorporateOfficial/CorporateDeleteUser", method = RequestMethod.GET)
	public ModelAndView CorporateDeleteUser(@ModelAttribute("emp") Employee emp, @ModelAttribute("employeeDummy") Employee employeeDummy) {
		ModelAndView model = new ModelAndView("/CorporateLevelManager/DeleteUser");
		model.addObject("emp",employeeService.RetrieveEmployeeDetails() );
		model.addObject("employeeDummy",employee );
		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS2");
		return model;
	}
	
	@RequestMapping("/WelcomeCorporateOfficial/CorporateDeleteEmployee")
	public ModelAndView CorporateDeleteEmployee(@ModelAttribute("employeeDummy") Employee empl )
	{
		employeeService.deleteEmployee(empl.getUsername());
		CorporateService.deleteUser(empl.getUsername());
		ModelAndView model = new ModelAndView("redirect:/WelcomeCorporateOfficial/CorporateDeleteUser");
		model.addObject("message","User Successfully deleted");
		return model;
	}

	@RequestMapping(value = "WelcomeCorporateOfficial/CorporateTransferUser", method = RequestMethod.GET)
	public ModelAndView Corporate_TransferUser() {
		Employee transferUser = new Employee();
		ModelAndView model = new ModelAndView("/CorporateLevelManager/Transfer_User");
		model.addObject("transferUser", transferUser);
		// log it via log4j

		if (logger.isInfoEnabled())
			logger.info("ENTERED CONTROLLER CLASS2");
		return model;
	}
	@RequestMapping("/WelcomeCorporateOfficial/CorporateModifyUser")
	public ModelAndView CorporateTransferUser(@RequestParam String UserID,
			String DepartmentId, String salary) throws HibernateException {
		try {
			if (UserID != null) {
				String msg = CorporateService.CorporateModifyInternalUser(UserID,
						DepartmentId, salary);
				if (msg.equalsIgnoreCase("error")) {
					return new ModelAndView("/accessDenied");
				}

				return new ModelAndView("/CorporateLevelManager/Transfer_User",
						"message", null);
			} else {
				return new ModelAndView("/accessDenied");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("/accessDenied");
		}
	}

	@RequestMapping("/WelcomeCorporateOfficial/CorporateViewAccountInfo")
	public ModelAndView viewAccountInfo_submit(HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("I'm in controller first line");
		HttpSession httpSession = request.getSession();
		httpSession.setAttribute("userName", SecurityContextHolder.getContext()
				.getAuthentication().getName());
		String UserID = (String) httpSession.getAttribute("userName");
		List<Account> msg = null;
		System.out.println("I'm in controller");
		try {
			if (UserID != null) {
				msg = (List<Account>) CorporateService.viewAccountInfo(UserID);

				return new ModelAndView("/CorporateLevelManager/View_Account_Info",
						"msg", msg);
			} else {
				return new ModelAndView("/accessDenied");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("/accessDenied");
		}
	}	
	
	@RequestMapping("/WelcomeCorporateOfficial/CorporateViewTransactions")
	public ModelAndView viewTransactions_submit(HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession httpSession = request.getSession();
		httpSession.setAttribute("userName", SecurityContextHolder.getContext()
				.getAuthentication().getName());
		String UserID = (String) httpSession.getAttribute("userName");
		List<Transaction> msg = null;
		try {
	
				msg = (List<Transaction>) CorporateService.viewTransactions(UserID);

				return new ModelAndView("/CorporateLevelManager/ViewTransactions",
						"msg", msg);
			
		
		} catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("/accessDenied");
		}
	}
	
	
	@RequestMapping("/WelcomeCorporateOfficial/CorporateViewPII")
	public ModelAndView viewPII() {

		List<User> msg = null;
		try {
			msg = (List<User>) CorporateService.viewPII();
			return new ModelAndView("/CorporateLevelManager/ViewPII", "msg", msg);
			// if (logger.isInfoEnabled())logger.info("ENTERED view PII");

		} catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("/accessDenied");
		}
	}
	
	@RequestMapping(value = "/WelcomeCorporateOfficial/CorporateAuthorize", method = RequestMethod.GET)
	public ModelAndView authorize(@ModelAttribute("transactionModel") Transaction transactionModel, ModelMap model) {
		List<Transaction> msg = null;
		try {
			msg = (List<Transaction>) CorporateService.authorize();
			model.addAttribute("transactionModel", transaction);
			model.addAttribute("model2", msg);
			return new ModelAndView("CorporateLevelManager/TransactionList", model);
			// if (logger.isInfoEnabled())logger.info("ENTERED view PII");

		} catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("/accessDenied");
		}
		}
	
	@RequestMapping(value = "/WelcomeCorporateOfficial/CorporateAuthorize", method = RequestMethod.POST)
	public ModelAndView authorizePost(@ModelAttribute("transactionModel") Transaction transaction) {
		Transaction trans = CorporateService.approveTransaction(transaction.getTransactionId());
		if(trans.getStatus().equals("processed"))
			return new ModelAndView("CorporateLevelManager/TransactionApproved");
		else 
			return new ModelAndView("CorporateLevelManager/TransactionApprovalRejected");
	}
	}
