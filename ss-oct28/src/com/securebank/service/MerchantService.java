package com.securebank.service;

import com.securebank.dao.MerchantDAO;

public class MerchantService {

	private MerchantDAO merchantDAO;

	public void setMerchantDAO(MerchantDAO merchantDAO) {
		this.merchantDAO = merchantDAO;
	}
	
	public String enableAuth(String userName) {
		return merchantDAO.enableAuth(userName);
	}

	public String disableAuth(String userName) {
		return merchantDAO.disableAuth(userName);
	}
}
