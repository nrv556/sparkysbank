package com.securebank.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.securebank.dao.EmployeeDAO;
import com.securebank.model.Employee;
import com.securebank.model.Transaction;
import com.securebank.model.User;

/**
 * 
 * @author Sowmya
 *
 */
public class EmployeeService {

	private EmployeeDAO employeeDAO;
	
	@Autowired
	public void setEmployeeDAO(EmployeeDAO employeeDAO) {
		this.employeeDAO = employeeDAO;
	}
	/**
	 * To verify whether the user belongs to the Transaction monitoring department or not.
	 * @param userName
	 */
	public List<Transaction> verifyEmployeeDepartment(String userName) {
		// TODO Auto-generated method stub
		 return employeeDAO.verifyEmployeeDepartment(userName);
	}
	public List<Transaction> retreiveTransactionDetails(Integer transactionId) {
		// TODO Auto-generated method stub
		return employeeDAO.retreiveTransactionDetails(transactionId);
	}
	public void updateModifiedTransaction(Integer transactionId,
			String updateAmount) {
		// TODO Auto-generated method stub
		employeeDAO.updateModifiedTransaction(transactionId, updateAmount);
	}
	public void deleteTransaction(Integer transactionId) {
		// TODO Auto-generated method stub
		employeeDAO.deleteTransaction(transactionId);
	}
	public void approveTransaction(Integer transactionId) {
		// TODO Auto-generated method stub
		employeeDAO.approveTransaction(transactionId);
	}
	public void createTransfer(Transaction transaction) throws Exception {
		// TODO Auto-generated method stub
		employeeDAO.createTransfer(transaction);
	}
	public void createCredit(Transaction transaction) {
		// TODO Auto-generated method stub
		employeeDAO.createCredit(transaction);
	}
	public void createDebit(Transaction transaction) throws Exception {
		// TODO Auto-generated method stub
		employeeDAO.createDebit(transaction);
	}
	public void authorizeSystemAdmin(String userName) {
		// TODO Auto-generated method stub
		employeeDAO.authorizeSystemAdmin(userName);
	}
	public void unauthorizeSystemAdmin(String userName) {
		// TODO Auto-generated method stub
		employeeDAO.unauthorizeSystemAdmin(userName);
	}
	
	public void updateUserProfile(User user, String attribute) throws Exception{
		// TODO Auto-generated method stub
		employeeDAO.updateUserProfile(user, attribute);
	}
		
	@Transactional
	public List<Employee> RetrieveEmployeeDetails(){
		return employeeDAO.RetrieveEmployeeDetails();
	}
	
	public void deleteEmployee(String username){
		employeeDAO.deleteEmployee(username);
	}
	
	@Transactional
	public List<Employee> displayResults() {
		// TODO Auto-generated method stub
		return employeeDAO.displayResults();
	}
	
	@Transactional
	public void deleteUser(String username){
		employeeDAO.deleteUser(username);
	}
	public long getAccountId(String username) {
		// TODO Auto-generated method stub
		return employeeDAO.getAccountId(username);
	}

}
