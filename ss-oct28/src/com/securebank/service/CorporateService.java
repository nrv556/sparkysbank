package com.securebank.service;



import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.securebank.dao.CorporateDAO;
import com.securebank.model.Account;
import com.securebank.model.Transaction;
import com.securebank.model.User;
@Transactional
@Service
public class CorporateService {
	@Autowired
	private CorporateDAO CorporateDAO;

	@Autowired
	public void setCorporateDAO(CorporateDAO CorporateDAO) {
		this.CorporateDAO = CorporateDAO;
	}

	public String addUser(String UserID, String Name, String Password,
			String dateOfBirth, String email, String address,
			String phoneNumber, String accountType, String securityQuestion,
			String Answer, String Role,String SavingAccount) throws HibernateException {
		try {
			System.out.println("DEBUG in service" + Password);
			long phoneNo = Long.parseLong(phoneNumber);
			Double savingAccount= Double.parseDouble(SavingAccount);
		
			
			System.out.println("DEBUG in service2" + phoneNumber);
			String message = CorporateDAO.addUser(UserID, Name, Password, dateOfBirth, email, address, phoneNo,securityQuestion, Answer,Role);
			CorporateDAO.createAccount(UserID, Role, savingAccount);
			CorporateDAO.createCustomer(UserID);
			// Hibernate Modification :: calls Sample code to populate Account
			// Model class and create the table.
			System.out.println("DEBUG in service3" + dateOfBirth);
			return message;
		} catch (Exception e) {
			e.printStackTrace();

		}
		return null;
	}

	public String addInternalUser(String UserID, String Name, String Password,
			String dateOfBirth, String email, String address,
			String phoneNumber,  String securityQuestion,
			String Answer, String Role,String DepartmentId,String Salary) throws HibernateException {
		try {
			System.out.println("DEBUG in service" + Password);
			long phoneNo = Long.parseLong(phoneNumber);
			Double salary= Double.parseDouble(Salary);
			Integer departmentId= Integer.parseInt(DepartmentId);
		
			
			System.out.println("DEBUG in service2" + phoneNumber);
			Password=DigestUtils.shaHex(Password);
			
			String message = CorporateDAO.addUser(UserID, Name, Password,dateOfBirth, email, address, phoneNo,securityQuestion, Answer,Role);
			 message= CorporateDAO.createEmployee(UserID, departmentId, salary);
			// Hibernate Modification :: calls Sample code to populate Account
			// Model class and create the table.
			System.out.println("DEBUG in service3" + dateOfBirth);
			return message;
		} catch (Exception e) {
			e.printStackTrace();

		}
		return null;
	}
//	//@Transactional
//	public String RetrieveUserDetails(){
//		return CorporateDAO.RetrieveUserDetails();
//	}
	
	public void deleteUser(String username){
		CorporateDAO.deleteUser(username);
	}
	
	public String CorporateModifyInternalUser(String username, String departmentId,
			String salary) {
		return CorporateDAO.CorporateUpdateInternalUser(username, departmentId, salary);
	}
	
	
	public List<Account> viewAccountInfo(String UserID) {
		
		List<Account> message = (List<Account>) CorporateDAO.viewAccountInfo(UserID);
				return message;
			}
	
public List<Transaction> viewTransactions(String UserID) {
		
		List<Transaction> message = (List<Transaction>) CorporateDAO.viewTransactions(UserID);
				return message;
			}


public List<User> viewPII() {
	
	List<User> message = (List<User>) CorporateDAO.viewPII();
			return message;
		}

public List<Transaction> authorize() {
	// TODO Auto-generated method stub
	List<Transaction> message = (List<Transaction>) CorporateDAO.authorize();
	return message;
}

public Transaction approveTransaction(Integer transactionId) {
	// TODO Auto-generated method stub
	return CorporateDAO.approveTransaction(transactionId);
}

}

