package com.securebank.service;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;

import com.securebank.dao.DepartmentDAO;
import com.securebank.model.Account;
import com.securebank.model.Transaction;
import com.securebank.model.User;

/**
 * 
 * @author Sandeep
 * 
 */

public class DepartmentService {
	// private UserDAO userDAO;
	private DepartmentDAO departmentDAO;

	@Autowired
	public void setDepartmentDAO(DepartmentDAO departmentDAO) {
		this.departmentDAO = departmentDAO;
	}

	/*public String addUser(String UserID, String Name, String Password,
			String accountType, String dateOfBirth, String email,
			String address, long phoneNumber, String securityQuestion,
			String Answer, String Role, String SavingAccount,
			String accountStatus) throws HibernateException,
			NumberFormatException {
		String text = null;
		try {
			long phoneNo = phoneNumber;
			@SuppressWarnings("unused")
			Double savingAccount = Double.parseDouble(SavingAccount);

			System.out.println("DEBUG in service2" + phoneNumber);
			String message = departmentDAO.addUser(UserID, Name, Password,
					dateOfBirth, email, address, phoneNo, securityQuestion,
					Answer, Role);
			// departmentDAO.createAccount(UserID, Role, savingAccount);

			if (Role.equalsIgnoreCase("User")) {
				// message = departmentDAO.createCustomer(UserID);
			}

			System.out.println("DEBUG in service3" + dateOfBirth);
			return message;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			text = "Please Enter digits properly";
		}

		catch (Exception e) {
			e.printStackTrace();
			text = "User is not added";
		}
		return text;
	}
*/
	public String addInternalUser(String UserID, String Name, String Password,
			String dateOfBirth, String email, String address, String phoneNumber,
			String securityQuestion, String Answer, String Role,
			String DepartmentId, String Salary) throws HibernateException {
		String message = null;
		try {

			Double salary = Double.parseDouble(Salary);
			Integer departmentId = Integer.parseInt(DepartmentId);
			Password = DigestUtils.shaHex(Password);
			long phoneNo = Long.parseLong(phoneNumber);
			
			 message = departmentDAO.addUser(UserID, Name, Password,
					dateOfBirth, email, address, phoneNo, securityQuestion,
					Answer, Role);
			message = departmentDAO
					.createEmployee(UserID, departmentId, salary);
			// Hibernate Modification :: calls Sample code to populate Account
			// Model class and create the table.
			return message;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			message = "Please enter the digits properly";
		} catch (Exception e) {
			e.printStackTrace();
			message = "Employee is not added";
		}
		return message;
	}

	public void deleteUser(String username) throws HibernateException {
		String text = null;
		try {
			departmentDAO.deleteUser(username);
			text = "User deleted successfully";
		} catch (Exception e) {
			e.printStackTrace();
			text = "Cannot delete the user";
		}
	}

	public String modifyInternalUser(String username, String departmentId,
			String salary) throws HibernateException {
		return departmentDAO.updateInternalUser(username, departmentId, salary);
	}

	public List<Account> viewAccountInfo(String UserID) {
		List<Account> message = (List<Account>) departmentDAO
				.viewAccountInfo(UserID);
		return message;
	}

	public List<Transaction> viewTransactions(String UserID) {
		List<Transaction> message = (List<Transaction>) departmentDAO
				.viewTransactions(UserID);
		return message;
	}

	public List<User> viewPII() {
		List<User> message = (List<User>) departmentDAO.viewPII();
		return message;
	}
	/* Authorize */
	public List<Transaction> authorize() {
		// TODO Auto-generated method stub
		List<Transaction> message = (List<Transaction>) departmentDAO.authorize();
		return message;
	}

	public Transaction approveTransaction(Integer transactionId) {
		// TODO Auto-generated method stub
		return departmentDAO.approveTransaction(transactionId);
	}
}
