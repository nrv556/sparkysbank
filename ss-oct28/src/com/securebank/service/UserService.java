package com.securebank.service;

import java.util.List;

import com.securebank.dao.UserDAO;
import com.securebank.model.Account;
import com.securebank.model.Transaction;
import com.securebank.model.User;

public class UserService {

	private UserDAO userDAO;

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public User searchByName(String username) {
		return userDAO.findSampleByName(username);
	}

	public String changePW(String username, String oldPW, String newPW,
			String confirmPW) {
		return userDAO.changePW(username, oldPW, newPW, confirmPW);
	}
	
	public String changePW(String username, String newPW) {
		return userDAO.changePW(username, newPW);
	}

	public String updateProfile(String username, String password,
			String newAddress, String email, String mobileNumber) {
		return userDAO.updateProfile(username, password, newAddress, email,
				mobileNumber);
	}

	public List<Transaction> getTransaction(String username) {
		return userDAO.getTransaction(username);
	}

	public double getBalance(String username) {
		return userDAO.getBalance(username);
	}
	
	public long getAccountId(String username) {
		return userDAO.getAccountId(username);
	}

	public Account getSenderAccount(String userName) {
		return userDAO.getSenderAccount(userName);
	}

	public String authorizeAccount(List<Transaction> list) {
		return userDAO.authorizeAccount(list);
	}

	public String payBills(String userName, long AccountNo, double Amount,
			String CompanyName) {
		return userDAO.payBills(userName, AccountNo, Amount, CompanyName);
	}

	public String credit(String userName, double CreditAmount) {
		return userDAO.credit(userName, CreditAmount);
	}

	public String debit(String userName, double DebitAmount) {
		return userDAO.debit(userName, DebitAmount);
	}

	public String transfer(String userName, String receiverID, String Name,
			double Amount) {
		return userDAO.transfer(userName, receiverID, Name, Amount);
	}

	public String enableAuth(String userName) {
		return userDAO.enableAuth(userName);
	}

	public String disableAuth(String userName) {
		return userDAO.disableAuth(userName);
	}

//	public String sendRequest(String userName, String receiverId, double Amount) {
//		return userDAO.sendRequest(userName, receiverId, Amount);
//	}
}
